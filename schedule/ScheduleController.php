<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
require_once(CLASS_DIR.'/schedule/Schedule.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/schedule/ScheduleView.php');


class ScheduleController extends Controller
{
  private $model;
  private $view;

  function __construct() {
    parent::__construct();
    $Ym = filter_input(INPUT_GET, 'm', FILTER_SANITIZE_SPECIAL_CHARS);
    $this->model = new Calender($this->getUserId(), $Ym);


    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
    switch ($action) {
      case 'register':
        $this->register();
        break;

      case 'remove':
      $this->removeEvents();
        break;
    }

    $this->view = new ScheduleView();
  }

  private function register() {
    $Ydm = filter_input(INPUT_POST, 'day', FILTER_SANITIZE_SPECIAL_CHARS);
    $remark = null;
    $tag = [];
    try {
      $schedule = new Schedule($this->getUserId(), $Ydm);
      $schedule->registerPersonalEvent($Ydm, 'マイ予定', $remark, $tag);
    } catch (\Exception $e) {

    }
  }

  private function removeEvents() {
    $Ydm = filter_input(INPUT_POST, 'day', FILTER_SANITIZE_SPECIAL_CHARS);
    $eventId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    try {
      $schedule = new Schedule($this->getUserId(), $Ydm);
      $schedule->removePersonalEvent($Ydm, $eventId);
    } catch (\Exception $e) {
      var_dump($e->getMessage());
    }

  }


  public function getYear() {
    return (int)$this->model->getYear();
  }

  public function getMonth() {
    return (int)$this->model->getMonth();
  }


  public function getPreviousMonth() {
    return $this->model->getPreviousMonth();
  }

  public function getNextMonth() {
    return $this->model->getNextMonth();
  }


  public function contents() {
    $id   = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    $Ym   = filter_input(INPUT_GET, 'm',  FILTER_SANITIZE_SPECIAL_CHARS);
    $date = filter_input(INPUT_GET, 'd',  FILTER_SANITIZE_SPECIAL_CHARS);
    $attr = filter_input(INPUT_GET, 'at', FILTER_SANITIZE_SPECIAL_CHARS);

    $isValidId   = preg_match('/^\d+$/', $id);
    $isValidYm   = preg_match('/^(\d{4})(0[1-9]|1[012])$/', $Ym, $YmMatches);
    $isValidDate = preg_match('/^(0[1-9]|[12]\d|3[01])+$/', $date);
    $isValidAttr = is_string($attr) && preg_match('/^[a-z]+$/', $attr);

    if ($isValidId && $isValidYm && $isValidDate & $isValidAttr) {
      $year  = $YmMatches[1];
      $month = $YmMatches[2];
      $this->view->specificEvent(parent::getUserId(), $year, $month, $date, (int)$id, $attr);

    } else {
      $this->view->scheduleTable(parent::getUserId(), $Ym);
    }
  }

}
