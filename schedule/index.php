<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/schedule/ScheduleController.php');
$controller = new ScheduleController();
?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>スケジュール</h1>

    <h2><?php echo $controller->getYear();?>年</h2>
    <h2><?php echo $controller->getMonth();?>月</h2>

    <div id="next">
      <form method="get" action="">
        <input type="hidden" name="m" value="<?php echo $controller->getPreviousMonth(); ?>">
        <input type="submit" value="前月">
      </form>
    </div>
    <div id="prev">
      <form method="get" action="">
        <input type="hidden" name="m" value="<?php echo $controller->getNextMonth(); ?>">
        <input type="submit" value="翌月">
      </form>
    </div>

    <?php $controller->contents(); ?>


  </body>
</html>
