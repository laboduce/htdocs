<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
require_once(CLASS_DIR.'/schedule/Calender.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/schedule/CalenderView.php');


class CalenderController extends Controller
{
  private $model;
  private $view;

  function __construct() {
    parent::__construct();
    $month = filter_input(INPUT_GET, 'm', FILTER_SANITIZE_SPECIAL_CHARS);

    $this->model = new Calender(parent::getUserId(), $month);
    $day = [['SUN', '日'], ['MON', '月'], ['TUE', '火'], ['WED', '水'], ['THU', '木'], ['FRI', '金'], ['SAT', '土']];

    $this->view = new CalenderView($this->model);
  }

  public function getYear() {
    return $this->model->getYear();
  }

  public function getMonth() {
    return $this->model->getMonth();
  }

  public function getPreviousMonth() {
    return $this->model->getPreviousMonth();
  }

  public function getNextMonth() {
    return $this->model->getNextMonth();
  }

  public function calenderTable() {
    $this->view->calenderTable();
  }
}
