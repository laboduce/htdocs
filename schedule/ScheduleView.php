<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/schedule/Calender.php');
require_once(CLASS_DIR.'/schedule/Schedule.php');


class ScheduleView
{

  public function scheduleTable($userId, $month) {
    $days = ['日', '月', '火', '水', '木', '金', '土'];
    $calender = new Calender($userId, $month);
    $lastDate = $calender->getLastDate();
    $day = $calender->getFirstDay();

    $year = $calender->getYear();
    $month = $calender->getMonth();
    echo '<div>';
    echo '<hr>';
    for ($date=1; $date <=  $lastDate; $date++, $day++) {
      $events = $calender->getEvent($year, $month, sprintf('%02d', $date));
      echo '<div>';// 1日ここから
      echo '<div><span>'.$date.'</span><span>('.$days[$day % 7].')</span></div>'; // 左

      echo '<div>'; // 右ここから
      echo '<ul>';
      foreach ($events as $event) {
        echo '<li><a href="?m='.$year.$month.'&d='.sprintf('%02d', $date).'&id='.$event->id.'&at='.$event->attr.'">'.$event->event.'</a>';
        if ($event->editable === 1) {
          echo '<form action="" method="POST">';
          echo '<input type="hidden" name="action" value="remove">';
          echo '<input type="hidden" name="day" value="'.$year.$month.sprintf('%02d', $date).'">';
          echo '<input type="hidden" name="id" value="'.$event->id.'">';
          echo '<input type="submit" value="削除">';
          echo '</form>';
        }
        echo '<div><ul>';
        foreach ($event->tag as $t) {
          echo '<li><a href="?t='.$t.'">'.$t.'</a></li>';
        }
        echo '</ul></div></li>';
      }
      echo '</ul>';

      echo '<form action="" method="POST">';
      echo '<input type="hidden" name="action" value="register">';
      echo '<input type="hidden" name="day" value="'.$year.$month.sprintf('%02d', $date).'">';
      echo '<input type="submit" value="追加">';
      echo '</form>';

      echo '</div>'; // 右ここまで
      echo '</div>'; // 1日ここまで


      echo '<hr>';
    }
    echo '</div>';
  }




  public function specificEvent($userId, $year, $month, $date, $id, $type) {
    $schedule = new Schedule($userId, $year.$month.$date);
    try {
      $event = $schedule->getSpecificEvent($id, $type);

    } catch (myexception\OperationException $e) {
      echo '<h2>該当するイベントが見つかりませんでした。</h2>';
      return;
    }
    $days = ['日', '月', '火', '水', '木', '金', '土'];

    echo '<div><span>'.$year.'年</span><span>'.$month.'月</span><span>'.$date.'日</span></div>';

    echo '<div>'.$event->event.'</div>';

    if (!is_null($event->remark)) {
      echo '<div>'.$event->remark.'</div>';
    } else {
      echo '<div></div>';
    }
    echo '<ul>';
    foreach ($event->tag as $t) {
      echo '<li><a href="?t='.$t.'">'.$t.'</a></li>';
    }
    echo '</ul>';
  }


}
