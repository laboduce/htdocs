<?php


class CalenderView
{
  private $model;

  function __construct($model) {
    $this->model = $model;
  }

  public function calenderTable() {
    $day = [['SUN', '日'], ['MON', '月'], ['TUE', '火'], ['WED', '水'], ['THU', '木'], ['FRI', '金'], ['SAT', '土']];
    echo '<table><thead><tr>';
    echo '<th class="sun">'.$day[0][0].'</th>';
    for ($i=1; $i < 7; $i++) {
      echo '<th>'.$day[$i][0].'</th>';
    }
    echo '</tr></thead>';
    echo '<tbody><tr>';
    for ($i=0; $i < $this->model->getFirstDay(); $i++) {
      echo '<td><span class="date"></span></td>'."\n";
    }
    $year = $this->model->getYear();
    $month = $this->model->getMonth();

    $lastDate = $this->model->getLastDate();
    $firstDay = $this->model->getFirstDay();

    for ($date=1; $date <= $lastDate ; $date++) {
      if (($firstDay + $date) % 7 === 1) {
        echo "</tr>\n<tr>";
      }
      $formatedDate = sprintf('%02d', $date);
      $cnt = count($this->model->getEvent($year, $month, $date));
      if ($this->model->isToday($year, $month, $date)) {
        echo '<td class="today"><a href="./?m='.$year.$month.'&d='.$formatedDate.'" target="_top"><span class="date">'.$date.'</span><br><span>'.$cnt.'</span></a></td>'."\n";
      } else {
        if ($this->model->isHoliday($year, $month, $date)) {
          echo '<td><a href="./?m='.$year.$month.'&d='.$formatedDate.'" target="_top"><span class="date">'.$date.'*</span><br><span>'.$cnt.'</span></a></td>'."\n";
        } else {
          echo '<td><a href="./?m='.$year.$month.'&d='.$formatedDate.'" target="_top"><span class="date">'.$date.'</span><br><span>'.$cnt.'</span></a></td>'."\n";
        }
      }
    }

    while (($firstDay + $date) % 7 !== 1) {
      echo '<td><span class="date"></span></td>'."\n";
      $date++;
    }
    echo '</tr></tbody></table>';
  }




}
