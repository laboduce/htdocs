<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/schedule/CalenderController.php');
$controller = new CalenderController();

?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="calender.css">
  <title>calender</title>
</head>

<body>
  <h3>
    <span class="year"><?php echo $controller->getYear();?></span>
    <span class="month"><?php echo $controller->getMonth();?></span>
  </h3>
  <div class="clearfix">
    <div id="next">
      <form method="get" action="">
        <input type="hidden" name="m" value="<?php echo $controller->getPreviousMonth(); ?>">
        <input type="submit" value="前月">
      </form>
    </div>
    <div id="prev">
      <form method="get" action="">
        <input type="hidden" name="m" value="<?php echo $controller->getNextMonth(); ?>">
        <input type="submit" value="翌月">
      </form>
    </div>
  </div>

  <?php $controller->calenderTable(); ?>

</body>

</html>
