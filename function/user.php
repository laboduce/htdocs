<?php
namespace func\user;
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/MyPDO.php');


function nameToId($name) {
  $pdo = new \MyPDO();
  $sql = "SELECT `id` FROM `user` WHERE `name` = :name;";
  $stmt = $pdo->prepare($sql);
  if (!$stmt->execute(['name' => $name])) {
    return false;
  }
  $id = $stmt->fetchColumn();
  if (!is_string($id)) {
    return null;
  }
  return $id;
}


function idToName($userId) {
  $pdo = new \MyPDO();
  $sql = "SELECT `name` FROM `user` WHERE `id` = :id;";
  $stmt = $pdo->prepare($sql);
  if (!$stmt->execute(['id' => $userId])) {
    return false;
  }
  $name = $stmt->fetchColumn();
  if (!is_string($name)) {
    return null;
  }
  return $name;
}


function getFriendsData($userId) {
  $pdo = new \MyPDO();
  $sql = "SELECT `friends`, `requested_friends`, `requesting_friends` FROM `user` WHERE `id` = :id;";
  $stmt = $pdo->prepare($sql);
  if (!$stmt->execute(['id' => $userId])) {
    return false;
  }

  $data = $stmt->fetch();
  $friends          = $data['friends'];
  $requestedFriend  = $data['requested_friends'];
  $requestingFriend = $data['requesting_friends'];

  $friendsArr    = [];
  $requestedArr  = [];
  $requestingArr = [];

  if (is_string($friends) && $friends !== "") {
    $friendsArr = explode(',', substr($friends, 0, -1));
  }
  if (is_string($requestedFriend) && $requestedFriend !== "") {
    $requestedArr = explode(',', substr($requestedFriend, 0, -1));
  }
  if (is_string($requestingFriend) && $requestingFriend !== "") {
    $requestingArr = explode(',', substr($requestingFriend, 0, -1));
  }

  $friendsData = [
    'friends'    => $friendsArr,
    'requested'  => $requestedArr,
    'requesting' => $requestingArr,
  ];
  return $friendsData;
}
