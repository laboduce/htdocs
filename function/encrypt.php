<?php
namespace func\encrypt;
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(SSL_PROPERTIES_PATH);


function encrypt($data, $encodeFunc='json_encode', $paramsArr=[JSON_UNESCAPED_UNICODE]) {
  array_unshift($paramsArr, $data);
  $str = call_user_func_array($encodeFunc, $paramsArr);
  if (!is_string($str)) {
    throw new \RuntimeException('エンコードに失敗。');
  }

  $iv_size = openssl_cipher_iv_length(OPENSSL_METHOD);
  $iv = openssl_random_pseudo_bytes($iv_size);
  $encrypted = openssl_encrypt($str, OPENSSL_METHOD, OPENSSL_PASSWORD, OPENSSL_OPTIONS, $iv);
  if (!is_string($encrypted)) {
    throw new \RuntimeException('暗号化に失敗。');
  }

  $base64_iv = base64_encode($iv);
  $base64_encrypted = base64_encode($encrypted);
  return ['base64_iv' => $base64_iv, 'base64_encrypted' => $base64_encrypted];
}



function decrypt($base64_iv, $base64_encrypted, $decodeFunc='json_decode', $paramsArr=[]) {
  $iv = base64_decode($base64_iv);
  $encrypted = base64_decode($base64_encrypted);
  $str = openssl_decrypt($encrypted, OPENSSL_METHOD, OPENSSL_PASSWORD, OPENSSL_OPTIONS, $iv);
  if (!is_string($str)) {
    throw new \RuntimeException('復号化に失敗。');
  }

  array_unshift($paramsArr, $str);
  $data = call_user_func_array($decodeFunc, $paramsArr);
  if (!$data) {
    throw new \RuntimeException('デコードに失敗。');
  }
  return $data;
}


// $filenameのファイルが存在しない場合は新規に作成する。
function saveEncryptedFile($filename, $data, $encodeFunc='json_encode', $paramsArr=[JSON_UNESCAPED_UNICODE]) {
  $base64 = encrypt($data, $encodeFunc, $paramsArr);
  $encryptedData = $base64['base64_iv']."\n".$base64['base64_encrypted'];

  if (file_put_contents($filename, $encryptedData, LOCK_EX) === false) {
    throw new \RuntimeException('ファイル('.DATA_DIR.'/group_chat/'.$this->groupId.'.json)の書き込みに失敗。');
  }
}


function loadEncryptedFile($filename, $decodeFunc='json_decode', $paramsArr=[]) {
  if (!file_exists($filename)) {
    throw new \RuntimeException('ファイル('.$filename.')が存在しなかった。');
  }

  $encryptedData = file_get_contents($filename);
  if (!is_string($encryptedData)) {
    throw new \RuntimeException('ファイル('.$filename.')の読み込みに失敗。');
  }
  $array = explode("\n", $encryptedData);
  if (count($array) !== 2) {
    throw new \RuntimeException('ファイル('.$filename.')の形式に異常があった。');
  }

  return decrypt($array[0], $array[1], $decodeFunc, $paramsArr);
}
