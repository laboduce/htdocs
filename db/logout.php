<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Identification.php');

$idAction = new Identification('logout');
$idAction->logout('/user/login/');
exit();
