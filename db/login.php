<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Identification.php');
if (!empty($_POST)) {
  if (!$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS)) {
    $error[] = 'name';
  }
  if (!$pw = filter_input(INPUT_POST, 'pw', FILTER_SANITIZE_SPECIAL_CHARS)) {
    $error[] = 'pw';
  }

  if (!isset($error)) {
    $idAction = new Identification('login');
    $idAction->checkPW($name, $pw);
    $destination = "/user/main/";
    if (!$idAction->login($destination)) {
      // TODO: 認証失敗の処理 jsで表示を変更とか
    }

  } else {
    // TODO; postの不正
    foreach ($error as $key => $value) {

    }
  }


} else {
  // TODO: 最初の場合
  // TODO: すでにログインの場合は？
}
