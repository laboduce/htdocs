<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>メイン</title>
    <link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon">
  </head>
  <body>
    <h1><?php echo (empty($_SERVER['HTTPS']) ? 'http://' : 'https://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?></h1>
    <a href="/user/login/">ログイン画面</a>

    <hr>
    <h1>>>[1-9]\d*のリンク化テスト</h1>
    <form action="" method="post">
      <textarea name="body" cols="50" rows="10" placeholder="コメント"></textarea>
      <input type="submit" value="投稿">
    </form>

    <?php
    if (isset($_POST['body'])) {
      if ($_POST['body']!=="") {
        $body = $_POST['body'];
        $body = preg_replace('/(?<=>>)([1-9]\\d*)/', '<a href="?$1">$1</a>', $body);
        $body = preg_replace('/(?<=\n)/', '<br>', $body);
        echo "<p>".$body."</p>\n";
      } else {
        echo "<p>入力なし</p>\n";
      }
    } else {
      echo "<p>POSTなし</p>\n";
    }
    ?>

    <hr>
    <h1>暗号化テスト</h1>
    <?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
    require_once(SSL_PROPERTIES_PATH);


    $data = '秘密の文章';

    // 暗号化
    $iv_size = openssl_cipher_iv_length(OPENSSL_METHOD);
    $iv = openssl_random_pseudo_bytes($iv_size);

    //暗号化
    $encrypted = openssl_encrypt($data, OPENSSL_METHOD, OPENSSL_PASSWORD, OPENSSL_OPTIONS, $iv);
    $base64_iv = base64_encode($iv);
    $base64_encrypted = base64_encode($encrypted);

    // 復号
    $iv = base64_decode($base64_iv);
    $encrypted = base64_decode($base64_encrypted);
    $decrypted = openssl_decrypt($encrypted, OPENSSL_METHOD, OPENSSL_PASSWORD, OPENSSL_OPTIONS, $iv);

    var_dump($decrypted);

    ?>

    <hr>
    <h1>テスト</h1>

  </body>
</html>
