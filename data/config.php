<?php
/*
外部からはアクセスされたくないデータ類の保存場所をここで管理する。
*/

$dir = $_SERVER['DOCUMENT_ROOT'];

// クラスを定義しているディレクトリ
define('CLASS_DIR', $dir.'/class');

// 関数を定義しているディレクトリ
define('FUNCTION_DIR', $dir.'/function');

// データベース
define('DB_PROPERTIES_PATH', $dir.'/absolute_secrecy/db/properties.php');

// ユーザーデータを保存するディレクトリ
$userDir = '/データ(本番はルートの外)';
define('DATA_DIR', $dir.$userDir);

// ユーザーデータを取得するディレクトリのURL
// TODO: 見られたくないデータと、見られてもいいデータは違うディレクトリにすべき？？？
define('DATA_URL', $userDir);


// SSL暗号
define('SSL_PROPERTIES_PATH', $dir.'/absolute_secrecy/ssl/properties.php');



define('SECHEDULE_DIR', $dir.'/schedule/data');
