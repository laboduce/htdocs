<?php require_once($_SERVER['DOCUMENT_ROOT'].'/user/register/signUp.php'); ?>


<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>承認画面</title>
  </head>


  <body>
    <?php
    if (!isset($error)) {
      echo "<h2>登録完了</h2>\n";
    } else {
      echo "<h2>登録失敗</h2>\n";
      var_dump($error);
    }
    ?>
    <a href="../login/">ログイン画面</a>

  </body>
</html>
