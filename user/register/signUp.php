<?php
if (!empty($_POST)) {
  if (!$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS)) {
    $error[] = 'name';
  } elseif (preg_match('/^[\s　]*$/', $name)){
    $error[] = 'name';
  }

  if (!$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS)) {
    $error[] = 'email';
  }

  if (!$pw = filter_input(INPUT_POST, 'pw', FILTER_SANITIZE_SPECIAL_CHARS)) {
    $error[] = 'pw';
  } elseif (preg_match('/^[\s　]*$/', $pw)) {
    $error[] = 'pw';
  }

  if (!$univ = filter_input(INPUT_POST, 'univ', FILTER_SANITIZE_SPECIAL_CHARS)) {
    if ($univ !== '0') {
      $error[] = 'univ';
    }
  }
  if (!$gakubu = filter_input(INPUT_POST, 'gakubu', FILTER_SANITIZE_SPECIAL_CHARS)) {
    if ($gakubu !== '0') {
      $error[] = 'gakubu';
    }
  }
  if (!$gakka = filter_input(INPUT_POST, 'gakka', FILTER_SANITIZE_SPECIAL_CHARS)) {
    if ($gakka !== '0') {
      $error[] = 'gakka';
    }
  }

  if (!isset($error)) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
    require_once(CLASS_DIR.'/Identification.php');
    $idAction = new Identification('signin');
    $data = [
      'pw' => $pw,
      'name' => $name,
      'email' => $email
    ];

    try {
      $idAction->signUp($data);
      echo "<h1>登録成功</h1>";
    } catch (myexception\DBException $e) {
      switch ($e->getCode()) {
        case  1001:
          $error[] = 'identity_name';
          break;

        default:
          $error[] = 'database';
          break;
      }
    }
  }

} else {
  $error[] = 'POST';
}
