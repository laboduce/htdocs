<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
class MainController extends Controller
{
  function __construct() {
    parent::__construct();
  }


  // view 部分
  public function echoUserId() {
    echo parent::getUserId();
  }

  public function echoUserName() {
    echo parent::getUserName();
  }

}
