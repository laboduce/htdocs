<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/user/main/MainController.php');
$controller = new MainController();
?>

<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>ログイン後のメイン画面</h1>
    <h2>"こんにちは。<a href="../profile/?id=<?php $controller->echoUserId() ?>"><?php $controller->echoUserName()?></a>さん。</h2>
    <h3>
      <a href="/db/logout.php">ログアウト</a><!--   href を表示してしまうのは？？   -->
    </h3>
    <ul>
      <li><a href="../contents/discussionboard/">掲示板</a></li>
      <li><a href="../contents/chat/">チャット</a></li>
      <li><a href="../contents/friends/">しりあい</a></li>
    </ul>

    <iframe style="overflow: auto; height: 430px" frameborder="0" src="/schedule/calender.php"></iframe>
  </body>
</html>
