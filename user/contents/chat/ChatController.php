<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/user/contents/chat/view.php');

require_once(CLASS_DIR.'/chat/PrivateChat.php');
require_once(CLASS_DIR.'/chat/GroupChat.php');
require_once(CLASS_DIR.'/chat/ChatList.php');

class ChatController extends Controller
{
  private $id;
  private $model;
  private $view;


  function __construct() {
    parent::__construct();
    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
    $this->id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    $type = filter_input(INPUT_GET, 't', FILTER_SANITIZE_SPECIAL_CHARS);


    if ($type === 'group') {
      $this->model = new GroupChat(parent::getUserId());
    } elseif ($type === 'private') {
      $this->model = new PrivateChat(parent::getUserId());
    } else {
      $this->model = new ChatList(parent::getUserId());
    }

    if ($this->model instanceof GroupChat || $this->model instanceof PrivateChat) {
      try {
        $this->model->setRoomId($this->id);
      } catch (\Exception $e) {
        $this->model = null;
        return;
      }
    }

    switch ($type) {
      case 'group':
        // グループのみ
        switch ($action) {
          case 'delete':
          // OK
            $this->deleteGroupAction();
            break;

          case 'withdraw':
          // OK
            $this->withdrawAction($this->id);
            break;

          case 'invite':
          // OK
            $this->inviteAction();
            break;

        }
      case 'private':
        // グループとプライベート共通
        switch ($action) {
          case 'post':
            $this->postAction();
            break;

          case 'like':
            $this->likeAction();
            break;
        }
        break;

      default:
        switch ($action) {
          case 'join':
            $this->joinAction();
            break;
          case 'create_new':
            $this->createGroupAction();
            break;
          case 'withdraw':
            $groupId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
            $this->withdrawAction($groupId);
            break;
        }
        break;
    }

    $this->view = new View($this->model);
  }


  private function postAction() {
    $message = filter_input(INPUT_POST, 'body', FILTER_SANITIZE_SPECIAL_CHARS);
    $messageId = filter_input(INPUT_POST, 'to', FILTER_SANITIZE_SPECIAL_CHARS);

    try {
      if (!is_string($messageId) || $messageId === '') {
        $this->model->postMessage($message);// 投稿
      } else {
        $this->model->replyMessage($message, $messageId);
      }
    } catch (myexception\InputException $e) {
      // TODO: 入力が不正
    } catch (myexception\AuthorityException $e) {

    } catch (\Exception $e) {

    }
  }

  private function likeAction() {
    $messageId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    try {
      $this->model->pressLike($messageId);
    } catch (myexception\OperationException $e) {
      // TODO: いいねしたことある
    } catch (myexception\AuthorityException $e) {

    } catch (\RuntimeException $e) {

    } catch (\Exception $e) {

    }
  }

  private function inviteAction() {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
    $message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_SPECIAL_CHARS);

    try {
      $this->model->inviteMember($name, $message);// メンバー招待
    } catch (myexception\OperationException $e) {
      // TODO: ユーザーが存在しない
      // TODO: すでに参加済
      // TODO: すでに招待済
    } catch (myexception\InputException $e) {
      // TODO: 入力の不正
    } catch (myexception\AuthorityException $e) {

    } catch (\Exception $e) {

    }
  }

  private function joinAction() {
    $groupId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    try {
      $this->model->joinGroup($groupId);
      header("Location:/user/contents/chat/?t=group&id={$groupId}");
      exit();
    } catch (myexception\InputException $e) {

    } catch (myexception\OperationException $e) {
      // TODO: 招待されていない
    } catch (\Exception $e) {

    }
  }

  private function createGroupAction() {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_SPECIAL_CHARS);
    try {
      $newGroupId = $this->model->createGroup($name, $description); // グループ作成
      header("Location:/user/contents/chat/?t=group&id={$newGroupId}");
      exit();
    } catch (myexception\InputException $e) {
      // TODO: 入力値が不正
    } catch (\Exception $e) {

    }
  }

  private function withdrawAction($groupId) {
    try {
      $this->model->withdrawGroup($groupId);
      header("Location:/user/contents/chat/");
      exit();
    } catch (myexception\InputException $e) {

    } catch (myexception\AuthorityException $e) {
      // TODO: 管理者は退会できない
    } catch (\Exception $e) {

    }
  }

  private function deleteGroupAction() {
    try {
      $this->model->deleteGroup();// グループ削除
      header("Location:/user/contents/chat/");
      exit();
    } catch (myexception\AuthorityException $e) {
      // TODO: 管理者以外が削除しようとした
    } catch (\Exception $e) {

    }
  }


  public function echoUserName() {
    echo parent::getUserName();
  }

  public function contents() {
    $page = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_SPECIAL_CHARS);
    $range = filter_input(INPUT_GET, 'r', FILTER_SANITIZE_SPECIAL_CHARS);

    if (is_null($this->model)) {
      var_dump("エラー(グループに参加していない・友達でない)");

    } elseif ($this->model instanceof GroupChat) {
      $this->view->groupMessages(parent::getUserId(), $page, $range, $this->id);

    } elseif ($this->model instanceof PrivateChat) {
      $this->view->privateMessages(parent::getUserId(), $page, $range);

    } elseif ($this->model instanceof ChatList) {
      $this->view->chatList(parent::getUserId());

    }
  }


}
