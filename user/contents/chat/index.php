<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/user/contents/chat/ChatController.php');
$controller = new ChatController();
?>

<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="/js/jquery-3.3.1.min.js" charset="utf-8"></script>
    <script src="/js/profile.js" charset="utf-8"></script>
    <link rel="stylesheet" href="/css/profile.css">
    <title>チャット</title>
  </head>
  <body>
    <section id="overlay" class="close">
      <div id="modalwindow">
        <input type="button" value="閉じる" class="close">
        <div></div>
      </div>
    </section>
    <h1>チャット</h1>
    <h2>こんにちは。<?php $controller->echoUserName();?> さん。</h2>
    <h2><a href="../../main/">メイン</a></h2>



    <?php
    // $filename = DATA_DIR.'/user/268837545c16473272454/chat/private/7053840105c258e7cc15c9.json';
    // $json = file_get_contents($filename);
    // echo "<h2>○検索テスト</h2>\n";
    // if (isset($_GET['search'])) {
    //   $search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_SPECIAL_CHARS);
    //   echo "<h2>→{$search}</h2>\n";
    //   preg_match_all('/(?<={"body":")([^"]*'.$search.'[^"]*?)","posted_date":"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}","from_id":"\w+?","name":"\w+?","message_id":(\d+)(?=})/', $json, $matches);
    //
    //   if (count($matches[1])===0) {
    //     echo "<h2>マッチなし</h2>\n";
    //   } else {
    //     echo "<ul>\n";
    //     $i = 0;
    //     foreach ($matches[1] as $key => $value) {
    //       if ($i++ > 5) break;
    //       echo "<li>No.{$matches[2][$key]}<br>　　$value</li>\n";
    //     }
    //     echo "</ul>\n";
    //     if ($i > 5) echo "<div>・・・</div>\n";
    //   }
    //
    // }
    //
    //
    //
    ?>



    <?php $controller->contents(); ?>

  </body>
</html>
