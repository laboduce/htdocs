<?php
define('DEFAULT_PAGE', 1);               // 現在のページのデフォルト値
define('DEFAULT_MESSAGE_RANGE', 15);     // メッセージ表示の時に1ページに表示するメッセージ数のデフォルト値
define('DEFAULT_LIST_RANGE', 10);        // 一覧表示の時に1ページに表示する掲示板の数のデフォルト値
define('DEFAULT_NAV_NUM', 5);            // ナビゲーションに表示するページの個数のデフォルト値
date_default_timezone_set('Asia/Tokyo');

class View
{
  private $model;
  function __construct($model) {
    $this->model = $model;
  }
  // 1ページにあたりのメッセージ数を取得
  private function getRange($range, $default) {
    if (preg_match('/^\d+$/', $range)) {
      $range = (int)$range;
      if ($range === 0) {
        $range = $default;
      }
    } else {
      $range = $default;
    }
    return (int)$range;
  }

  // ページ数の最大値を取得
  private function getMaxPage($count, $pageRange) {
    $maxPage = (int)ceil($count / $pageRange);
    return $maxPage;
  }

  // 現在のページ番号を取得
  private function getPage($page, $maxPage, $default) {
    if (preg_match('/^\d+$/', $page)) {
      $page = (int)$page;
      if ($page === 0) {
        $page = $default;
      }
      if ($page > $maxPage) {
        $page = $maxPage;
      }
    } else {
      $page = $default;
    }
    return (int)$page;
  }

  // 何番目のデータから表示するかを取得
  private function getBegine($range, $page) {
    $begine = $range * ($page - 1) + 1;
    return $begine;
  }

   /*===============================================================
  以下の html表示系のメソッドはサイトデザインが決定後調整が必要。
   ===============================================================*/


  public function groupMessages($userId, $page, $range, $groupId) {
    echo "<h2><a href=\"./\">リスト表示</a></h2>\n";
    $this->echoInviteForm($groupId);
    $this->echoGroupChatData($userId);
    $this->echoPostForm();
    $this->echoMessageList($page, $range, $userId);
  }


  public function privateMessages($userId, $page, $range) {
    echo "<h2><a href=\"./\">リスト表示</a></h2>\n";
    $this->echoPrivateChatData($userId);
    $this->echoPostForm();
    $this->echoMessageList($page, $range, $userId);
  }


  public function chatList($userId) {
    $participatingGroups = $this->model->getParticipatingGroups();
    $invitedGroups = $this->model->getInvitedGroups();
    $friends = $this->model->getFriends();

    $this->echoCreateGroupForm();
    echo "<h2>○個人リスト</h2>\n";
    $this->echoPrivateChatList($friends);
    echo "<h2>○グループリスト</h2>\n";
    $this->echoGroupChatList($participatingGroups, $invitedGroups, $userId);
  }





  private function echoPrivateChatList($privateChatList) {
    if (is_null($privateChatList) || count($privateChatList) === 0) {
      echo "<h3>なし</h3>\n";
      return;
    }

    $list = "<ul>\n";
    foreach($privateChatList as $chat) {
      $list .= "<li>";
      $list .= "<a href=\"?t=private&id={$chat->id}\">{$chat->name}</a>";
      $list .= "</li>\n";
    }
    $list .= "</ul>\n";
    echo $list;
  }

  private function echoGroupChatList($groupChatList, $invitedGroupList, $userId) {
    // 参加しているグループ
    if (is_null($groupChatList) || count($groupChatList) === 0) {
      echo "<h3>参加グループなし</h3>\n";
    } else {
      $list = "<ul>\n";
      $i = true;
      foreach ($groupChatList as $data) {
        $list .= "<li>\n";
        $list .= "<a href=\"?t=group&id={$data->group_id}\">{$data->group_name}</a>\n";
        $list .= "<ul>\n";
        $list .= "<li>作成者:{$data->master->name}</li>\n";
        $list .= "<li>参加者(数):".count(get_object_vars($data->member))."</li>\n";
        // $unread = $data->cnt - $data->last_read;
        // $list .= "<li>コメント数:{$data->cnt}({$unread}未読)</li>\n";
        $list .= "<li>コメント数:{$data->cnt}</li>\n";
        $list .= "<li>ID(これを入力すれば参加できる OR 招待):{$data->group_id}</li>\n";
        if (!is_null($data->description)) {
          $list .= "<li>グループの説明:{$data->description}</li>\n";
        }
        $list .= "<li>最後の投稿:{$data->last_posted_date}</li>\n";

        if ($data->master->id !== $userId) {
          $list .= "<li>\n";
          $list .= "<form action=\"\" method=\"post\">\n";
          $list .= "<input type=\"hidden\" name=\"action\" value=\"withdraw\">\n";
          $list .= "<input type=\"hidden\" name=\"id\" value=\"{$data->group_id}\">\n";
          $list .= "<input type=\"submit\" value=\"退会\">\n";
          $list .= "</form>\n";
          $list .= "</li>\n";
        }

        $list .= "</ul>\n</li>\n";
      }

      $list .= "</ul>\n";
      echo $list;
    }

    echo "<h2>招待されてるグループ</h2>\n";
    // 招待されているグループ
    if (is_null($invitedGroupList) || count($invitedGroupList) === 0) {
      echo "<h3>招待されているグループなし</h3>\n";
    } else {
      $list = "<ul>\n";
      foreach($invitedGroupList as $group) {
        $list .= "<li>\n";

        $list .= "<div>{$group->group_name}\n";
        $list .= "<div>by <a class=\"profile\" href=\"../../profile/?id={$group->inviter->id}&user={$userId}\">{$group->inviter->name}</a></div>\n";
        $list .= "<div>at {$group->invited_date}</div>\n";

        $list .= "<form action=\"\" method=\"post\">\n";
        $list .= "<input type=\"hidden\" name=\"action\" value=\"join\">\n";
        $list .= "<input type=\"hidden\" name=\"id\" value=\"{$group->group_id}\">\n";
        $list .= "<input type=\"submit\" value=\"参加\">\n";
        $list .= "</form>\n";

        $list .= "</div>\n";

        if (isset($group->message)) {
          $list .= "<div>メッセージ:{$group->message}</div>\n";
        }
        $list .= "</li>\n";
      }

      $list .= "</ul>\n";
      echo $list;
    }
  }


  private function echoMessageList($page, $range, $userId) {
    $this->model->setMessages();
    $count = $this->model->getMessageCount();
    $range = $this->getRange($range, DEFAULT_MESSAGE_RANGE);
    $maxPage = $this->getMaxPage($count, $range);
    $page = $this->getPage($page, $maxPage, DEFAULT_PAGE);
    $begine = $this->getBegine($range, $page);

    $messages = $this->model->getMessages($begine, $range);

    $list = "<ul>\n";
    foreach ($messages as $message) {
      $from_name = $message->name;
      $posted_date = date("Y/m/d H:i:s", $message->posted_date);

      $list .= "<br>\n";
      $list .= "<li>No.{$message->message_id}\n";
      $list .= "<ul>\n";
      $list .= "<li>投稿者:";
      if ($message->from_id !== -1) {
        $list .= "<a class=\"profile\" href=\"../../profile/?id={$message->from_id}&user={$userId}\">{$from_name}</a>";
      } else {
        $list .= $from_name;
      }
      $list .= "</li>\n";
      $list .= "<li>投稿日:{$posted_date}</li>\n";
      if (!is_null($message->to_id)) {
        $list .= "<li>トゥー:{$message->to_id}</li>\n";
      }
      $list .= "<li>本文：{$message->body}</li>\n";
      $list .= "<li>";
      $list .= '<form action="" method="post">';
      $list .= '<input type="hidden" name="action" value="like">';
      $list .= '<input type="hidden" name="id" value="'.$message->message_id.'">';
      $list .= '<input type="submit" value="いいね">('.$message->like.')';
      $list .= '</form>';
      $list .= "</li>";
      $list .= "</ul>\n";
      $list .= "</li>\n";
    }
    $list .= "</ul>\n";
    echo $list;
  }



  private function echoPostForm() {
    ?>
    <form action="" method="post">
      <input type="hidden" name="action" value="post">
      <textarea name="body" cols="50" rows="10" placeholder="コメントを書く"></textarea>
      <button type="submit" name="post_button">投稿</button>
    </form>
    <?php
  }

  private function echoCreateGroupForm() {
    ?>
    <form action="./" method="post">
      <input type="hidden" name="action" value="create_new">
      <input type="text" name="name" placeholder="新しいグループ名" size="50"><br>
      <textarea name="description" cols="50" rows="10" placeholder="このグループの説明"></textarea>
      <input type="submit" value="新規作成">
    </form>
    <?php
  }

  private function echoInviteForm($groupId) {
    ?>
    <form action="./?t=group&id=<?php echo $groupId;?>" method="post">
      <input type="hidden" name="action" value="invite">
      <input type="text" name="name" placeholder="招待するユーザー名" size="50"><br>
      <textarea name="message" cols="50" rows="5" placeholder="メッセージ"></textarea>
      <input type="submit" value="招待">
    </form>
    <?php
  }



  private function echoPrivateChatData($userId) {
    $chatData = $this->model->getRoomInfo();

    echo "<h1>相手：<a class=\"profile\" href=\"../../profile/?id={$chatData->companion->id}&user={$userId}\">".$chatData->companion->name."</a></h1>\n";
    // echo "<h2>{$chatData->last_read}まで読んだことある(echoChatData)(未読のメッセージをハイライトする)</h2>\n";
  }

  private function echoGroupChatData($userId) {
    $info = $this->model->getGroupInfo();

    if (isset($info->inviting)) {
      $inviting = $info->inviting;
    } else {
      $inviting = null;
    }

    $members = $info->member;
    $master = $info->master;

    echo "<hr>";

    if ($userId === $master->id) {
      ?>
      <h3>管理者</h3>
      <form action="" method="post">
        <input type="hidden" name="action" value="">
        グループ名：
        <input type="textarea" name="name" value="<?php echo $info->group_name;?>">
        <input type="submit" value="変更">
      </form>

      <form action="" method="post">
        <input type="hidden" name="action" value="">
        説明文：
        <textarea name="description" cols="50" rows="5" placeholder="変更後の説明文"></textarea>
        <input type="submit" value="変更">
      </form>

      <form action="" method="post">
        <input type="hidden" name="action" value="delete">
        <input type="submit" value="グループを削除">
      </form>
      <?php
    } else {
      ?>
      <h3>一般メンバー</h3>
      <form action="" method="post">
        <input type="hidden" name="action" value="withdraw">
        <input type="submit" value="グループを退会">
      </form>
      <?php
    }

    echo "<hr>";

    echo "<h2>↓招待中↓</h2>\n";
    $list = "<ul>\n";
    foreach($inviting as $id => $name) {
      $list .= "<li><a class=\"profile\" href=\"../../profile/?id={$id}&user={$userId}\">{$name}</a></li>\n";
    }
    $list .= "</ul>\n";
    echo $list;

    echo "<hr>";

    echo "<h2>↓メンバー↓</h2>\n";
    $list = "<ul>\n";
    foreach($members as $id => $name) {
      if ($id === $master->id) {
        $list .= "<li><a class=\"profile\" href=\"../../profile/?id={$id}&user={$userId}\">{$name}</a>(管理者)</li>\n";
      } else {
        $list .= "<li><a class=\"profile\" href=\"../../profile/?id={$id}&user={$userId}\">{$name}</a></li>\n";
      }
    }
    $list .= "</ul>\n";
    echo $list;

    echo "<hr>";
    echo "<h2>グループ名:{$info->group_name}</h2>\n";

    // echo "<h2>{$this->model->getLastRead()}まで読んだことある(echoChatData)(未読のメッセージをハイライトする)</h2>\n";
  }



}
