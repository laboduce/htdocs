<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
require_once(CLASS_DIR.'/discussionboard/DiscussionBoard.php');
require_once(CLASS_DIR.'/discussionboard/DiscussionBoardList.php');

class DisucussionBoardController extends Controller
{
  private $model;
  private $view;

  private $roomId;
  private $orderBy;
  private $category;
  private $isAbsCate;

  function __construct($destination="/discussionboard/") {
    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
    $this->roomId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);

    $url = parse_url($_SERVER['REQUEST_URI']);
    $nowPage = $url['path'];
    if (isset($url['query'])) {
      parse_str($url['query'], $parse);
      if (isset($parse['id'])) {
        $nowPage .= '?id='.$parse['id'];
      }
    }

    parent::__construct($nowPage, $destination);

    if (!$this->roomId) {
      // 掲示板一覧表示
      $this->model = new DiscussionBoardList($this->getUserId());

      $category = filter_input(INPUT_GET, 'category', FILTER_SANITIZE_SPECIAL_CHARS);
      $absCategory = filter_input(INPUT_GET, 'c', FILTER_SANITIZE_SPECIAL_CHARS);
      if (is_string($absCategory) && !preg_match('/^[\s　]*$/', $absCategory)) {
        $this->isAbsCate = true;
        $this->category = $absCategory;
      } else {
        $this->isAbsCate = false;
        $this->category = $category;
      }

      $this->orderBy = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_SPECIAL_CHARS);

    } else {
      // メッセージ表示
      $this->model = new DiscussionBoard($this->getUserId());
      try {
        $this->model->setRoomId($this->roomId);
      } catch (\Exception $e) {
        var_dump($e->getMessage());
      }
    }


    $this->view = new View($this->model);
  }


  public function echoUserName() {
    echo parent::getUserName();
  }

  public function echoUserId() {
    echo parent::getUserId();
  }

  public function contents() {
    $page = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_SPECIAL_CHARS);
    $range = filter_input(INPUT_GET, 'r', FILTER_SANITIZE_SPECIAL_CHARS);

    if ($this->model instanceof DiscussionBoardList) {
      $this->view->discussionBoardList($this->getUserId(), $this->orderBy, $this->category, $page, $range, $this->orderBy, $this->isAbsCate);

    } elseif ($this->model instanceof DiscussionBoard) {
      // $likes = $this->model->getLikedMessages($this->roomId, parent::getUserId());
      $this->view->messages($this->roomId, $this->getUserId(), $this->getUserName(), $page, $range);

    }
  }

}
