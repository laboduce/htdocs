<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/user/contents/discussionboard/view.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/user/contents/discussionboard/DiscussionBoardController.php');
$controller = new DisucussionBoardController();
?>

<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="/js/jquery-3.3.1.min.js" charset="utf-8"></script>
    <script src="/js/profile.js" charset="utf-8"></script>
    <script src="/js/reply.js" charset="utf-8"></script>
    <link rel="stylesheet" href="/css/profile.css">
    <title>掲示板</title>
  </head>
  <body>
    <section id="overlay" class="close">
      <div id="modalwindow">
        <input type="button" value="閉じる" class="close">
        <div></div>
      </div>
    </section>
    <h1>掲示板</h1>
    <h2>こんにちは。<?php $controller->echoUserName();?> さん。</h2>
    <h2><a href="../../main/">メイン</a></h2>


    <?php $controller->contents();?>


  </body>
</html>
