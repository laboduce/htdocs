<?php

define('DEFAULT_PAGE', 1);       // 現在のページのデフォルト値
define('DEFAULT_RANGE', 15);     // メッセージ表示の時に1ページに表示するメッセージ数のデフォルト値
define('DEFAULT_LIST_RANGE', 10);// 一覧表示の時に1ページに表示する掲示板の数のデフォルト値
define('DEFAULT_NAV_NUM', 5);    // ナビゲーションに表示するページの個数のデフォルト値
define('CATEGORY_RANK_CNT', 10);  // カテゴリランクするカテゴリ数

date_default_timezone_set('Asia/Tokyo');

class View
{
  private $model;
  function __construct($model) {
    $this->model = $model;
  }
  // 1ページにあたりのメッセージ数を取得
  private function getRange($range, $default) {
    if (preg_match('/^\d+$/', $range)) {
      $range = (int)$range;
      if ($range === 0) {
        $range = $default;
      }
    } else {
      $range = $default;
    }
    return (int)$range;
  }

  // ページ数の最大値を取得
  private function getMaxPage($count, $pageRange) {
    $maxPage = (int)ceil($count / $pageRange);
    return $maxPage;
  }

  // 現在のページ番号を取得
  private function getPage($page, $maxPage, $default) {
    if (preg_match('/^\d+$/', $page)) {
      $page = (int)$page;
      if ($page === 0) {
        $page = $default;
      }
      if ($page > $maxPage) {
        $page = $maxPage;
      }
    } else {
      $page = $default;
    }
    return (int)$page;
  }

  // 何番目のデータから表示するかを取得
  private function getBegine($range, $page) {
    $begine = $range * ($page - 1) + 1;
    return $begine;
  }


  public function discussionBoardList($userId, $orderBy, $category, $page, $range, $sort, $isAbsCate) {
    $count = $this->model->getRoomCount($category, $isAbsCate);
    $range = $this->getRange($range, DEFAULT_LIST_RANGE);
    $maxPage = $this->getMaxPage($count, $range);
    $page = $this->getPage($page, $maxPage, DEFAULT_PAGE);

    echo "<hr>\n";
    $this->echoCreateNewBoardForm();
    echo "<hr>\n";
    $this->echoSortSelector($isAbsCate, $category, $range, $sort);
    $this->echoCategorySearch();
    if ($isAbsCate) {
      $this->echoPopularCategory($category);
    } else {
      $this->echoPopularCategory(null);
    }


    echo "<hr>\n";
    $this->echoListPageNav($isAbsCate, $category, $page, $maxPage, $range, $sort);
    echo "<div>表示しているカテゴリ >>> {$category}</div>\n";
    echo "<h3>{$count}個の掲示板が見つかりました</h3>\n";

    $this->echoBoardList($userId, $page, $range, $orderBy, $category, $isAbsCate);
    $this->echoListPageNav($isAbsCate, $category, $page, $maxPage, $range, $sort);
  }


  public function messages($roomId, $userId, $userName, $page, $range) {
    $count = $this->model->getMessageCount();
    $range = $this->getRange($range, DEFAULT_LIST_RANGE);
    $maxPage = $this->getMaxPage($count, $range);
    $page = $this->getPage($page, $maxPage, DEFAULT_PAGE);

    $boardData = $this->model->getBoardData();

    echo "<h3><a href=\"./\">掲示板一覧</a></h3>";
    $this->echoManagerForm($userId, $userName, $boardData->master_id);
    $this->echoBoardData($boardData);
    $this->echoPostForm();
    echo "<h3>↓今の所、message_idが大きい順(新しい順)(変更できる！！)↓</h3>";
    echo "<hr>\n";
    $this->echoMessagePageNav($roomId, $page, $maxPage, $range);
    echo "<hr>\n";
    $this->echoMessageList($roomId, $userId, $page, $range);
    $this->echoMessagePageNav($roomId, $page, $maxPage, $range);
  }


  /*===============================================================
  以下の html表示系のメソッドはサイトデザインが決定後調整が必要。
   ===============================================================*/
  // 投稿フォーム
  private function echoPostForm() {
    ?>
    <form action="" method="post">
      <input type="hidden" name="action" value="post">
      返信先：<input type="text" name="to" size="10">
      <textarea name="body" cols="50" rows="10" placeholder="コメントを書く"></textarea>
      <input type="submit" value="投稿">
    </form>
    <?php
  }


  // 新規掲示板作成のためのフォーム
  private function echoCreateNewBoardForm(){
      ?>
      <form action="./" method="post">
        <input type="hidden" name="action" value="create">
        <div>
          新しい掲示板の名前：<input type="text" name="name" size="40">
        </div>
        <div>
          説明(任意)：
          <div>
            <textarea name="description" cols="50" rows="10" placeholder="この掲示板の説明"></textarea>
          </div>
        </div>
        <div>
          カテゴリー(任意)：<input type="text" name="category" size="40" placeholder="カテゴリー">
        </div>
        <input type="submit" value="新規作成">
      </form>
      <?php
  }

  private function echoSortSelector($isAbsCate, $category, $range, $sort) {
    if (!is_string($category) || preg_match('/^[\s　]*$/', $category)) {
      $category = null;
    }

    $mode = ['views' => '閲覧数',
             'comments' => 'コメント数順',
             'created' => '作成日順',
             'lastpost' => '投稿時間順'];
    if (!array_key_exists($sort, $mode)) {
      $sort = 'views';
    }

    $selector = "<ul>\n";
    if ($isAbsCate) {
      $paramCate = 'c';
    } else {
      $paramCate = 'category';
    }
    foreach ($mode as $key => $value) {
      if ($key === $sort) {
        $selector .= "<li>{$value}</li>\n";
      } else {
        if (is_string($category)) {
          $selector .= "<li><a href=\"?{$paramCate}={$category}&sort={$key}&r={$range}\">{$value}</a></li>\n";
        } else {
          $selector .= "<li><a href=\"?sort={$key}&r={$range}\">{$value}</a></li>\n";
        }

      }
    }
    $selector .= "</ul>\n";
    echo $selector;
  }

  private function echoCategorySearch() {
    ?>
    <form action="" method="get">
      <input type="text" name="category" size="40" placeholder="カテゴリー検索">
      <input type="submit" value="検索">
    </form>
    <?php
  }

  private function echoPopularCategory($category){
    if (!is_string($category) || preg_match('/^[\s　]*$/', $category)) {
      $category = null;
    }
    echo "<h2>人気のカテゴリー</h2>\n";
    $categories = $this->model->getCategoryRank(CATEGORY_RANK_CNT);
    $list = "<ol>\n";
    foreach ($categories as $cate) {
      if ($category === $cate->category) {
        $list .= "<li>{$cate->category}({$cate->cnt})</li>";
      } else {
        $c = urlencode($cate->category);
        $list .= "<li><a href=\"?c={$c}\">{$cate->category}({$cate->cnt})</a></li>";
      }
    }

    $list .= "</ol>\n";
    echo $list;
  }


  // 管理者？一般？
  private function echoManagerForm($userId, $userName, $masterId) {
    echo "<hr>\n";
    echo "<h2>ユーザー:{$userName}さん</h2>\n";
//    echo "<h2><a href=\"\">お気に入り</a></h2>\n";

    if ($userId === $masterId) {
      ?>
      <h2>この掲示板の作成者です</h2>
      <form action="" method="post">
        <input type="hidden" name="action" value="delete">
        <input type="submit" value="掲示板を削除">
      </form>

      <form action="" method="post">
        <input type="hidden" name="action" value="change_description">
        <textarea name="description" cols="50" rows="5" placeholder="この掲示板の説明"></textarea>
        <input type="submit" value="紹介文を変更">
      </form>
      <?php
    } else {
      echo "<h2>一般</h2>\n";
    }

    echo "<hr>";
  }

  // ページリングのナビゲーション
  // TODO: ページ数が多くなった時の処理
  private function echoListPageNav($isAbsCate, $category, $page, $maxPage, $range, $sort) {
    if (!is_string($category) || preg_match('/^[\s　]*$/', $category)) {
      $category = null;
    }

    $mode = ['views', 'comments', 'created', 'lastpost'];
    if(!in_array($sort, $mode, true)) {
      $sort = 'views';
    }

    if (!is_null($page)) {
      echo "<span>今のページ >>> $page</span>";
    }
    $pageNav = "<ul>\n";
    if ($isAbsCate) {
      $paramCate = 'c';
    } else {
      $paramCate = 'category';
    }
    for ($i = 1; $i <= $maxPage; $i++) {
      if ($i === $page) {
        $pageNav .= "<li>{$i}</li>\n";
      } else {
        if (is_string($category)) {
          $pageNav .= "<li><a href=\"?{$paramCate}={$category}&sort={$sort}&p={$i}&r={$range}\">{$i}</a></li>\n";
        } else {
          $pageNav .= "<li><a href=\"?&sort={$sort}&p={$i}&r={$range}\">{$i}</a></li>\n";
        }
      }
    }
    $pageNav .= "</ul>\n";
    echo $pageNav;
  }


  private function echoMessagePageNav($id, $page, $maxPage, $range) {
    echo "<span>今のページ >>> $page</span>\n";
    echo "<hr>\n"; // TODO: cssで書くべき

    if ($maxPage === 1) {
      return;
    }

    $pageNav = "<ul>\n";
    if (DEFAULT_NAV_NUM < $maxPage && $page > 1) {
      $pageNav .= "<li><a href=\"?id={$id}&p=1&r={$range}\">最初(1)</a></li>\n";
    }
    if ($maxPage <= DEFAULT_NAV_NUM) {
      for ($i = 1; $i <= $maxPage; $i++) {
        if ($i == $page) {
          $pageNav .= "<li>{$i}</li>\n";
          continue;
        }
        $pageNav .= "<li><a href=\"?id={$id}&p={$i}&r={$range}\">{$i}</a></li>\n";
      }

    } else {
      if ($page <= ceil(DEFAULT_NAV_NUM/2)) {
        $begine = 1;
        $end = $begine + DEFAULT_NAV_NUM - 1;
      } elseif ($page >= $maxPage - ceil(DEFAULT_NAV_NUM/2) + 1) {
        $begine = $maxPage - DEFAULT_NAV_NUM + 1;
        $end = $maxPage;
      } else {
        $begine = $page - ceil(DEFAULT_NAV_NUM/2) + 1;
        $end = $begine + DEFAULT_NAV_NUM - 1;
      }

      if ($begine !== 1) {
        $pageNav .= "<li>・・・</li>\n";
      }
      for ($i = $begine; $i <= $end; $i++) {
        if ($i == $page) {
          $pageNav .= "<li>{$i}</li>\n";
        } else {
          $pageNav .= "<li><a href=\"?id={$id}&p={$i}&r={$range}\">{$i}</a></li>\n";
        }
      }
      if ($end !== $maxPage) {
        $pageNav .= "<li>・・・</li>\n";
      }
    }

    if (DEFAULT_NAV_NUM < $maxPage && $page < $maxPage) {
      $pageNav .= "<li><a href=\"?id={$id}&p={$maxPage}&r={$range}\">最後({$maxPage})</a></li>\n";
    }
    $pageNav .= "</ul>\n";

    echo $pageNav;
  }

  // 投稿された投稿の一覧を表示
  private function echoMessageList($id, $userId, $page, $range) {
    $begine = $this->getBegine($range, $page);
    $likes = $this->model->preesedMessages();

    $datas = $this->model->getMessages($begine, $range);
    foreach ($datas as $data) {
      $posted_date = date("Y/m/d H:i:s", $data->posted_date);
      ?>

      <div id="<?php echo $data->message_id;?>" class="message">
        <ul>
          <li>No.<?php echo $data->message_id;?></li>
          <li>投稿日:<?php echo $posted_date?></li>
          <li>投稿者:
            <a class = "profile" href="../../profile/?id=<?php echo $data->from_id;?>&user=<?php echo $userId;?>"><?php echo $data->name;?></a>
          </li>

<?php
      if (!is_null($data->to_id)) {
?>
          <li>トゥー:<?php echo $data->to_id;?></li>
<?php
      }
?>
          <li>本文:<?php echo $data->body;?></li>
          <li><strong>返信ボタンを作る</strong></li>
          <li>
<?php

      if (is_array($likes) && in_array($data->message_id, $likes, true)) {
        $likes = array_diff($likes, [$data->message_id]);
        $likeForm = 'いいね(済)('.$data->like.')';
      } else {
        $likeForm = '<form action="" method="post">';
        $likeForm .= '<input type="hidden" name="action" value="like">';
        $likeForm .= '<input type="hidden" name="id" value="'.$data->message_id.'">';
        $likeForm .= '<input type="submit" value="いいね">('.$data->like.')';
        $likeForm .= '</form>';
      }
      echo $likeForm;

?>
          </li>
        </ul>
<?php

      if (!is_null($data->reply)) {
?>
        <div class="reply">
          <ul>
            <li>返信リスト</li>
<?php
        foreach ($data->reply as $replyId) {
          $reply = $this->model->readSpecificMessage($replyId);
          if (is_null($reply)) {
            continue;
          }
?>
            <ul>
              <li>message_id:<?php echo $reply->message_id;?></li>
              <li>from:<?php echo $reply->name;echo "(".$reply->from_id.")";?></li>
            </ul>
<?php
        }
?>
          </ul>
        </div>
<?php
      }
?>
        </div>
<?php
      echo "<hr>\n"; // TODO: cssで書くべき
    }
  }

  // 掲示板の一覧を表示
  private function echoBoardList($userId, $page, $range, $orderBy, $category, $isAbsCate) {
    $begine = $this->getBegine($range, $page);

    $datas = $this->model->getDiscussionBoards($begine, $range, $orderBy, $category, $isAbsCate);

    $section = "<ul>\n";
    echo $section;

    foreach ($datas as $data) {
      echo "<hr>\n"; // TODO: cssで書くべき
      $created_date = date("Y/m/d H:i:s", $data->created_date);
      $last_posted_date = date("Y/m/d H:i:s", $data->last_posted_date);
      $section = "<li><a href=\"?id={$data->board_id}\">{$data->board_name}</a>({$data->board_id})\n";
      $section .= "<div>\n";
      $section .= "説明文:{$data->description}\n";
      $section .= "<div>\n";
      $section .= "<div>\n";
      $section .= "<div>作成者";
      $section .= "<a  class=\"profile\" href=\"../../profile/?id={$data->master_id}&user={$userId}\">({$data->master_name})</a>";


      $section .= "</div>\n";
      $section .= "<div>作成日({$created_date})</div>\n";
      $section .= "<div>最後の投稿({$last_posted_date})</div>\n";
      $section .= "<div>コメント({$data->cnt})</div>\n";
      $section .= "<div>閲覧数({$data->views})</div>\n";
      if (is_array($data->categories) && count($data->categories) !== 0) {
        $section .= "<div>カテゴリ:\n";
        $section .= "<ul>\n";
        foreach ($data->categories as $category) {
          $section .= "<li><a href=\"?c={$category}\">{$category}</a></li>\n";
        }
        $section .= "</ul>\n";
        $section .= "</div>\n";
      }

      $section .= "</div>\n";
      $section .= "</li>\n";

      echo $section;
    }

    echo "<hr>\n"; // TODO: cssで書くべき
    $section = "</ul>\n";
    echo $section;
  }

  // 掲示板情報を表示
  private function echoBoardData($boardData) {
    $created_date = date("Y/m/d H:i:s", $boardData->created_date);
    $last_posted_date = date("Y/m/d H:i:s", $boardData->last_posted_date);

    $section = "<h3>掲示板情報</h3>\n<ul>\n<li>掲示板id:{$boardData->board_id}</li>\n";
    $section .= "<li>掲示板名:{$boardData->board_name}</li>\n";
    $section .= "<li>作成日時:{$created_date}</li>\n";
    $section .= "<li>最終投稿日時:{$last_posted_date}</li>\n";
    $section .= "<li>投稿数:{$boardData->cnt}</li>\n";
    $section .= "<li>閲覧数:{$boardData->views}</li>\n";
    $section .= "<li>紹介文:{$boardData->description}</li>\n";
    $section .= "<li>作成者:{$boardData->master_name}</li>\n";

    if (is_array($boardData->categories) && count($boardData->categories) !== 0) {
      $section .= "<li>カテゴリ(タグ):\n";
      $section .= "<ul>\n";
      foreach ($boardData->categories as $category) {
        $section .= "<li><a href=\"?c={$category}\">{$category}</a></li>\n";
      }
      $section .= "</ul>\n";
      $section .= "</li>\n";
    }

    $section .= "</ul>\n";
    echo $section;
    echo "<hr>\n";
  }


}
