<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
require_once(CLASS_DIR.'/Friends.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/user/contents/friends/View.php');

class FriendsController extends Controller
{
  private $model;
  private $view;

  function __construct() {
    parent::__construct();
    $this->model = new Friends(parent::getUserId());

    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    // TODO: 型チェックは friend クラスで

    if ($action === 'request') {
      try {
        $this->model->requestFriend($name);
      } catch (myexception\InputException $e) {

      } catch (myexception\OperationException $e) {
        // TODO: 存在しないユーザーに対して申請
        // TODO: 自分に対して申請
        // TODO: すでに知り合いリストの登録済

      } catch (\Exception $e) {

      }
    }

    if ($action === 'delete') {
      try {
        $this->model->deleteFriend($id);
      } catch (myexception\InputException $e) {

      } catch (\Exception $e) {

      }
    }

    if ($action === 'approval') {
      try {
        $this->model->approval($id);
      } catch (myexception\InputException $e) {

      } catch (\Exception $e) {

      }
    }

    if ($action === 'rejection') {
      try {
        $this->model->rejection($id);
      } catch (myexception\InputException $e) {

      } catch (\Exception $e) {

      }
    }

    if ($action === 'cancel') {
      try {
        $this->model->cancelRequest($id);
      } catch (myexception\InputException $e) {

      } catch (\Exception $e) {

      }
    }

    $this->model->getFriendsData();
    $this->view = new View();
  }

  public function echoUserName() {
    echo parent::getUserName();
  }


  public function contents() {
    $this->view->echoFriendList($this->model->getFriends(), parent::getUserId());
    $this->view->echoRequestedList($this->model->getRequestedFriend(), parent::getUserId());
    $this->view->echoRequestingList($this->model->getRequestingFriend(), parent::getUserId());
  }
}
