<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/user/contents/friends/FriendsController.php');
$controller = new FriendsController();
?>
<!DOCTYPE html>

<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="/js/jquery-3.3.1.min.js" charset="utf-8"></script>
    <script src="/js/profile.js" charset="utf-8"></script>
    <link rel="stylesheet" href="/css/profile.css">
    <title></title>
  </head>
  <body>
    <section id="overlay" class="close">
      <div id="modalwindow">
        <input type="button" value="閉じる" class="close">
        <div></div>
      </div>
    </section>
    <h1><?php $controller->echoUserName(); ?>さん</h1>
    <h2><a href="../../main/">メイン</a></h2>
    <h3>↓友達申請する(ユーザー名で)↓</h3>
    <form action="" method="post">
      <input type="text" name="name" size="40">
      <input type="hidden" name="action" value="request">
      <input type="submit" value="追加">
    </form>

    <?php $controller->contents();?>

  </body>
</html>
