<?php

class View
{
  /*===============================================================
  以下の html表示系のメソッドはサイトデザインが決定後調整が必要。
   ===============================================================*/
  public function echoFriendList($friends, $myUserId) {
    echo "<hr>\n<h3>↓知り合いリスト↓</h3>\n";
    if (count($friends) === 0) {
      echo "<h3>知り合いが一人もいません</h3>";
      return;
    }

    $list = "<ul>\n";
    foreach($friends as $friend) {
      $name = $friend->name;
      $userId = $friend->id;
      $list .= "<li>\n";
      $list .= "<a class=\"profile\" href=\"../../profile/?id={$userId}&user={$myUserId}\">{$name}</a>\n";
      $list .= "<form action=\"\" method=\"post\">\n";
      $list .= "<input type=\"hidden\" name=\"action\" value=\"delete\">\n";
      $list .= "<input type=\"hidden\" name=\"id\" value=\"{$userId}\">\n";
      $list .= "<input type=\"submit\" value=\"削除\">\n";
      $list .= "</form>\n";
      $list .= "<a href=\"../chat/?t=private&id={$userId}\">チャット</a>\n";

      $list .= "</li>\n";
    }
    $list .= "</ul>\n";
    echo $list;
  }

  public function echoRequestedList($friends, $myUserId) {
    echo "<hr>\n<h3>↓友達申請されているリスト↓</h3>\n";
    if (count($friends) === 0) {
      echo "<h3>あなたを申請している人はいません</h3>\n";
      return;
    }
    $list = "<ul>\n";
    foreach($friends as $friend) {
      $name = $friend->name;
      $userId = $friend->id;

      $list .= "<li>\n";
      $list .= "<a class=\"profile\" href=\"../../profile/?id={$userId}&user={$myUserId}\">{$name}</a>\n";

      $list .= "<form action=\"\" method=\"post\">\n";
      $list .= "<input type=\"hidden\" name=\"action\" value=\"approval\">\n";
      $list .= "<input type=\"hidden\" name=\"id\" value=\"{$userId}\">\n";
      $list .= "<input type=\"submit\" value=\"承認\">\n";
      $list .= "</form>\n";

      $list .= "<form action=\"\" method=\"post\">\n";
      $list .= "<input type=\"hidden\" name=\"action\" value=\"rejection\">\n";
      $list .= "<input type=\"hidden\" name=\"id\" value=\"{$userId}\">\n";
      $list .= "<input type=\"submit\" value=\"拒否\">\n";
      $list .= "</form>\n";
      $list .= "</li>\n";
    }
    $list .= "</ul>\n";
    echo $list;
  }

  public function echoRequestingList($friends, $myUserId) {
    echo "<hr>\n<h3>↓友達申請してるリスト↓</h3>\n";
    if (count($friends) === 0) {
      echo "<h3>あなたが申請してる人はいません</h3>";
      return;
    }
    $list = "<ul>\n";
    foreach($friends as $friend) {
      $name = $friend->name;
      $userId = $friend->id;
      $list .= "<li>\n";
      $list .= "<a class=\"profile\" href=\"../../profile/?id={$userId}&user={$myUserId}\">{$name}</a>\n";
      $list .= "<form action=\"\" method=\"post\">\n";
      $list .= "<input type=\"hidden\" name=\"action\" value=\"cancel\">\n";
      $list .= "<input type=\"hidden\" name=\"id\" value=\"{$userId}\">\n";
      $list .= "<input type=\"submit\" value=\"取消\">\n";
      $list .= "</form>\n";
      $list .= "</li>\n";
    }
    $list .= "</ul>\n";
    echo $list;
  }
}
