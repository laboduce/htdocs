<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/user/profile/ProfileController.php');
$controller = new ProfileController();
?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>プロフィール</title>
  </head>
  <body>
    <h1>ユーザー：<?php $controller->getUserName(); ?></h1>
    <h1>プロフィール</h1>
    <h2><a href="../main/">メイン</a></h2>

    <?php $controller->contents(); ?>



  </body>
</html>
