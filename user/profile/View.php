<?php
class View
{
  private $model;
  function __construct($model) {
    $this->model = $model;
  }

  public function myself($userName) {
    echo "<h1>自分</h1>";
    $profile = $this->model->getProfile();
    $this->echoEditForm($profile);
  }

  public function others() {
    echo "<h1>他人</h1>";
    $profile = $this->model->getProfile();
    $this->echoProfile($profile);
  }

  public function noUser() {
    echo "<h1>ユーザーがいない</h1>";
    $this->echoNoProfile();
  }



  private function echoEditForm($profile) {
    ?>
    <hr>
    <h3>ユーザー名：</h3>
    <form action="" method="post">
      <input type="hidden" name="action" value="name">
      <input type="text" name="name" value="<?php echo $profile['name'];?>">
      <input type="submit" value="変更">
    </form>

    <hr>
    <h3>アイコン：</h3>
    <img src="<?php echo $profile['iconURL'];?>" alt="アイコン">
    <form action="" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="icon">
      <input type="hidden" name="MAX_FILE_SIZE" value="1000000">
      <input type="file" name="image">
      <input type="submit" value="変更">
    </form>
    <hr>

    <h3>コメント：</h3>
    <form action="" method="post">
      <input type="hidden" name="action" value="comment">
      <textarea name="comment" rows="8" cols="80"><?php echo str_replace('<br>', '', $profile['comment']);?></textarea>
      <input type="submit" value="変更">
    </form>
    <hr>

    <?php
   }


  private function echoProfile($profile) {
    ?>
    <h2>プロフィール</h2>
    <img src="<?php echo $profile['iconURL'];?>" alt="アイコン">
    <?php
    var_dump($profile);
  }


  private function echoNoProfile() {
    echo "<h2>プロフィールデータがない</h2>\n";
  }
}
