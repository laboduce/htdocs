<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Controller.php');
require_once(CLASS_DIR.'/Profile.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/user/profile/View.php');


class ProfileController extends Controller
{
  private $model;
  private $view;
  private $id;

  function __construct() {
    parent::__construct(null, '/profile/');

    $this->id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    $this->model = new Profile($this->id, parent::getUserId());

    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
    if ($action === 'icon') {
      try {
        $this->model->editIcon();
      } catch (myexception\AuthorityException $e) {
        // 自分以外
      } catch (myexception\InputException $e) {
        switch ($e->getCode()) {
          case 1001:
            // TODO: ファイルが大きすぎる
            break;
          case 1002:
            // TODO: 画像以外のファイル
            break;
          case 1003:
            // TODO: ファイルが送信されなかった
            break;
        }
        // TODO: 受信時のエラー
      } catch (\RuntimeException $e) {

      } catch (\Exception $e) {

      }
    }


    if ($action === 'name') {
      $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
      try {
        $this->model->editName($name);
      } catch (myexception\AuthorityException $e) {
        // 自分以外
      } catch (myexception\InputException $e) {
        // TODO: 入力
      } catch (myexception\OperationException $e) {
        // TODO: 同じユーザー名
      } catch (myexception\DBException $e) {
        // TODO: すでに登録されたユーザー名
      } catch (\RuntimeException $e) {

      } catch (\Exception $e) {

      }
    }

    if ($action === 'comment') {
      $comment = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_SPECIAL_CHARS);
      try {
        $this->model->editComment($comment);
      } catch (myexception\AuthorityException $e) {
        // 自分以外
      } catch (myexception\InputException $e) {
        // 入力
      } catch (myexception\OperationException $e) {
        // TODO: 同じコメント
      } catch (\RuntimeException $e) {

      } catch (\Exception $e) {

      }
    }


    $this->view = new View($this->model);
  }


  public function contents() {
    if (is_null($this->id)) {
      echo "idがない";

    } elseif ($this->model->getIsMe()) {
      $this->view->myself(parent::getUserName());

    } elseif (!is_null($this->model->getProfile())) {
      $this->view->others();
    } else {
      $this->view->noUser();
    }



  }

  public function getUserName() {
    echo parent::getUserName();
  }

}
