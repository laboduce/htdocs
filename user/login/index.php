<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/db/login.php');
 ?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>ログイン</title>
    <link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/strong.css">
    <script type="text/javascript" src="login.js">

    </script>
  </head>

  <body>
    <header>
      <h1 class="logo">
        <a href="/">マッチング</a>
      </h1>

      <nav>
        <ul class="navi">
          <li><a href="#">このサイトについて</a></li>
          <li class="current"><a href="#">ログイン</a></li>
          <li><a href="/user/register/">新規登録</a></li>
          <li><a href="#">お問い合わせ</a></li>
        </ul>

        <h2>コンテンツ(ゲスト)</h2>
        <select onchange="jump(this)" style="width:220px;"><!-- cssで -->
          <option value>選択してください</option>
          <optgroup label="コンテンツ">
            <option value="discussionboard">掲示板</option>
            <option value="profile">プロフィール</option>
          <optgroup label="aaaa">

          </optgroup>
          </optgroup>
        </select>

      </nav>
    </header>

    <div class="divider"></div>

    <main>
      <h2>ログイン</h2>
      <form action="" method="post" id="login_form">
        <table>
          <tr>
            <th id="name">ユーザー名</th>
            <td>
              <input class="inputbox" maxlength="40" name="name" size="20" type="text">
            </td>
          </tr>
          <tr>
            <th id="pw">パスワード</th>
            <td>
              <input class="inputbox" maxlength="50" name="pw" size="20" type="password">
            </td>
          </tr>
          <tr class="button">
            <td>
              <button class="inputbox" type="button" name="login_button" onclick="check()">ログイン</button>
            </td>
            <td>
            </td>
          </tr>
        </table>
      </form>
    </main>

    <script type="text/javascript" src="/js/login_check.js"></script>
  </body>
</html>
