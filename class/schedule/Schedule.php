<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(FUNCTION_DIR.'/encrypt.php');
require_once(CLASS_DIR.'/Exceptions.php');


class Schedule
{
  private $day;
  private $univEvents;
  private $personalEvents;
  private $userId;

  function __construct($userId, $day='now') {
    $this->userId = $userId;
    $this->setDay($day);
    $this->loadUnivEvents();
    $this->loadPrersonalEvents();
  }

  public function setDay($day='now') {
    $isYmd = is_string($day) && preg_match('/^\d{8}$/', $day);
    if ($isYmd) {
      $this->day = new DateTime($day);
    } else {
      $this->day = new DateTime();
    }
  }


  public function registerPersonalEvent($Ydm, $event, $remark=null, $tag=[]) {
    if (!is_string($event) || preg_match('/^[\s　]*$/', $event)) {
      throw new myexception\InputException('入力されたイベントの内容が不適切($event:'.$event.')');
    }

    if (!preg_match('/^(\d{4})(\d{2})(\d{2})$/', $Ydm, $matches)) {
      throw new myexception\InputException('入力された日付が不適切('.$Ydm.')');
    }
    $year  = $matches[1];
    $month = $matches[2];
    $date  = $matches[3];

    $newEvent = new stdClass();
    $newEvent->event    = $event;
    $newEvent->remark   = $remark;
    $newEvent->tag      = $tag;
    $newEvent->editable = 1;
    $newEvent->id       = 0;

    if (isset($this->personalEvents->$year->$month->$date)) {
      $lastId = (function ($array) {
        $id = count($array) === 0 ? 0 : end($array)->id;
        return $id;
      })($this->personalEvents->$year->$month->$date);
      $newEvent->id = ++$lastId ;
    }

    if (!isset($this->personalEvents->$year)) {
      $this->personalEvents->$year = new stdClass();
    }
    if (!isset($this->personalEvents->$year->$month)) {
      $this->personalEvents->$year->$month = new stdClass();
    }
    if (!isset($this->personalEvents->$year->$month->$date)) {
      $this->personalEvents->$year->$month->$date = [];
    }

    $this->personalEvents->$year->$month->$date[] = $newEvent;
    $filename = DATA_DIR.'/user/'.$this->userId.'/schedule.json';
    \func\encrypt\saveEncryptedFile($filename, $this->personalEvents);
  }

  public function removePersonalEvent($Ydm, $eventId) {
    if (!preg_match('/^\d+$/', $eventId)) {
      throw new myexception\InputException('入力されたイベントのIDの値が不適切('.$eventId.')');
    }
    if (!preg_match('/^(\d{4})(\d{2})(\d{2})$/', $Ydm, $matches)) {
      throw new myexception\InputException('入力された日付が不適切('.$Ydm.')');
    }
    $year  = $matches[1];
    $month = $matches[2];
    $date  = $matches[3];

    if (!isset($this->personalEvents->$year->$month->$date)) {
      throw new myexception\OperationException('存在しない日付にアクセスしようとした。');
    }

    $events = $this->personalEvents->$year->$month->$date;
    foreach ($events as $key => $event) {
      if ($event->id === (int)$eventId) {
        if ($event->editable === 0) {
          throw new myexception\OperationException('編集権限のない予定を削除しようとしている。');
        }
        array_splice($this->personalEvents->$year->$month->$date, $key, 1);
        $filename = DATA_DIR.'/user/'.$this->userId.'/schedule.json';
        \func\encrypt\saveEncryptedFile($filename, $this->personalEvents);
        return;
      }
    }
    throw new myexception\OperationException('存在しないeventIdを指定した。');
  }


  private function loadPrersonalEvents() {
    $userDir = DATA_DIR.'/user/'.$this->userId;
    $filename = $userDir.'/schedule.json';
    try {
      $this->personalEvents = \func\encrypt\loadEncryptedFile($filename);
    } catch (\Exception $e) {
      if (!file_exists($userDir)) {
        mkdir($userDir);
      }
      $personalEvents = (object)[];
      try {
        \func\encrypt\saveEncryptedFile($filename, $personalEvents);
      } catch (\Exception $e) {

      }
      $this->personalEvents = $personalEvents;
    }
  }

  private function loadUnivEvents() {
    $univ = 'keio';
    // TODO: univ が未実装
    // $_db = new DataBase('`user`');
    // $where = ["`id`='".$this->userId."'"];
    // $column = ['`uviv`'];
    // $_db->select($column, $where);


    $filename = DATA_DIR.'/schedule/'.$univ.'.json';
    if (!file_exists($filename)) {
      return;
    }
    $json = file_get_contents($filename);
    if (!is_string($json)) {
      return;
    }
    $events = json_decode($json);
    if ($events) {
      $this->univEvents = $events;
    }
  }


  public function getEvent() {
    $year  = $this->day->format('Y');
    $month = $this->day->format('m');
    $date  = $this->day->format('d');

    $events = [];
    if (isset($this->univEvents->$year->$month->$date)) {
      $univEvents = $this->univEvents->$year->$month->$date;
      $univEvents = array_map(function($event) {$event->attr='univ'; return $event;}, $univEvents);
      $events = array_merge($univEvents);
    }

    if (isset($this->personalEvents->$year->$month->$date)) {
      $personalEvents = $this->personalEvents->$year->$month->$date;
      $personalEvents = array_map(function($event) {$event->attr='personal'; return $event;}, $personalEvents);
      $events = array_merge($events, $personalEvents);
    }
    return $events;
  }


  public function getSpecificEvent($id, $attr) {
    $events = $this->getEvent();
    foreach ($events as $event) {
      if ($event->attr === $attr && $event->id === $id) {
        return $event;
      }
    }
    throw new myexception\OperationException('存在しないイベントを取得しようとした。');

  }



}
