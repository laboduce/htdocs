<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/schedule/Schedule.php');
date_default_timezone_set('Asia/Tokyo');

class Calender
{
  private $targetDay;
  private $userId;

  function __construct($userId, $time) {
    $this->userId = $userId;
    if (is_string($time) && preg_match('/^(\d{4})(0[1-9]|1[012])$/', $time, $matches)) {
      $year  = $matches[1];
      $month = $matches[2];

    } else {
      $today = new DateTime();
      $year  = $today->format('Y');
      $month = $today->format('m');
    }
    $this->targetDay = new DateTime($year.$month.'01');
  }

  public function getYear() {
    return (int)$this->targetDay->format('Y');
  }

  public function getMonth() {
    return (int)$this->targetDay->format('m');
  }

  public function getNextMonth() {
    $year  = (int)$this->targetDay->format('Y');
    $month = (int)$this->targetDay->format('m');

    if ($month === 12) {
      $nextYear  = $year + 1;
      $nextMonth = 1;
    } else {
      $nextYear  = $year;
      $nextMonth = $month + 1;
    }

    $nextYm = sprintf('%04d%02d', $nextYear, $nextMonth);
    return $nextYm;
  }

  public function getPreviousMonth() {
    $year  = (int)$this->targetDay->format('Y');
    $month = (int)$this->targetDay->format('m');

    if ($month === 1) {
      $previousYear  = $year - 1;
      $previousMonth = 12;
    } else {
      $previousYear  = $year;
      $previousMonth = $month -1;
    }

    $previousYm = sprintf('%04d%02d', $previousYear, $previousMonth);
    return $previousYm;
  }

  public function getLastDate() {
    $lastDate = $this->targetDay->format('t');
    return (int)$lastDate;
  }

  public function getFirstDay() {
    $firstDay = $this->targetDay->format('w');
    return (int)$firstDay;
  }


  public function isToday($year, $month, $date) {
    $today = new DateTime();// 約30回呼ばれる。
    $isThisYear  = $year  === (int)$today->format('Y');
    $isThisMonth = $month === (int)$today->format('m');
    $isThisDate  = $date  === (int)$today->format('d');
    if ($isThisYear && $isThisMonth && $isThisDate) {
      return true;
    }
    return false;
  }

  public function getEvent($year, $month, $date) {
    $schedule = new Schedule($this->userId);
    $schedule->setDay(sprintf('%04d%02d%02d', $year, $month, $date));
    return $schedule->getEvent();
  }



  public function isHoliday($year, $month, $date) {
    switch ($month) {
      case 1:
        // 元日
        if ($date === 1) {
          return true;
        }
        if ($date === 2 && $this->isSunday($year, $month, 1)) {
          return true;
        }
        // 成人の日
        if ($date === $this->getFirstMonday($year, $month) + 7) {
          return true;
        }
        break;
      case 2:
        // 建国記念の日
        if ($date === 11) {
          return true;
        }
        if ($date === 12 && $this->isSunday($year, $month, 11)) {
          return true;
        }
        // 天皇誕生日
        if ($year > 2019) {
          if ($date === 23) {
            return true;
          }
          if ($date === 24 && $this->isSunday($year, $month, 23)) {
            return true;
          }
        }
        break;
      case 3:
        // 春分の日
        $day1 = (int)(20.8431 + 0.242194 * ($year - 1980)) - (INT)(($year - 1980) / 4);
        if ($date === $day1) {
          return true;
        }
        if ($date === $day1 + 1 && $this->isSunday($year, $month, $day1)) {
          return true;
        }
        break;
      case 4:
        // 昭和の日
        if ($date === 29) {
          return true;
        }
        if ($date === 30 && $this->isSunday($year, $month, 29)) {
          return true;
        }
        if ($year === 2019 && $date === 30) {
          return true;
        }
        break;
      case 5:
        // 天皇の即位
        if ($year === 2019) {
          if ($date === 1 || $date === 2) {
            return true;
          }
        }
        // 憲法記念日
        if ($date === 3) {
          return true;
        }
        // みどりの日
        if ($date === 4) {
          return true;
        }
        // こどもの日
        if ($date === 5) {
          return true;
        }
        if ($date === 6 && ($this->isSunday($year, $month, 3) ||
                            $this->isSunday($year, $month, 4) ||
                            $this->isSunday($year, $month, 5))) {
          return true;
        }
        break;
      case 6:
        //
        break;
      case 7:
        // 海の日
        if ($year === 2020) {
          if ($date === 23) {
            return true;
          }
        } else {
          if ($date === $this->getFirstMonday($year, $month) + 14) {
            return true;
          }
        }

        break;
      case 8:
        // 山の日
        if ($year === 2020) {
          if ($date === 10) {
            return true;
          }
        } else {
          if ($date === 11) {
            return true;
          }
          if ($date === 12 && $this->isSunday($year, $month, 11)) {
            return true;
          }
        }
        break;
      case 9:
        // 敬老の日
        if ($date === $this->getFirstMonday($year, $month) + 14) {
          return true;
        }
        // 秋分の日
        $day2 = (int)(23.2488 + 0.242194 * ($year - 1980)) - (int)(($year - 1980) / 4);
        if ($date === $day2) {
          return true;
        }
        if ($date === $day2 + 1 && $this->isSunday($year, $month, $day2)) {
          return true;
        }
        break;
      case 10:
        // 即位の礼正殿の儀
        if ($year === 2019) {
          if ($date === 22) {
            return true;
          }
        }
        // 体育の日
        if ($date === $this->getFirstMonday($year, $month) + 7) {
          return true;
        }
        break;
      case 11:
        // 文化の日
        if ($date === 3) {
          return true;
        }
        if ($date === 4 && $this->isSunday($year, $month, 3)) {
          return true;
        }
        // 勤労感謝の日
        if ($date === 23) {
          return true;
        }
        if ($date === 24 && $this->isSunday($year, $month, 23)) {
          return true;
        }
        break;
      case 12:
        // 天皇誕生日
        if ($year < 2019) {
          if ($date === 23) {
            return true;
          }
          if ($date === 24 && $this->isSunday($year, $month, 23)) {
            return true;
          }
        }
        break;
    }
    return false;
  }


  private function getFirstMonday($year, $month) {
    for ($date=1; $date < 8; $date++) {
      $dt = new DateTime(sprintf('%04d%02d%02d',$year, $month, $date));
      if($dt->format('w') === '1') {
        return $date;
      }
    }
  }

  public function isSunday($year, $month, $date) {
    $dt = new DateTime(sprintf('%04d%02d%02d',$year, $month, $date));
    if ($dt->format('w') === '0') {
      return true;
    }
    return false;
  }

  public function isSaturday($year, $month, $date) {
    $dt = new DateTime(sprintf('%04d%02d%02d',$year, $month, $date));
    if ($dt->format('w') === '6') {
      return true;
    }
    return false;
  }


}
