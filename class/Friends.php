<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/MyPDO.php');
require_once(FUNCTION_DIR.'/user.php');

class UserData {
  public $id;
  public $name;

  function __construct($id, $name) {
    $this->id = $id;
    $this->name = $name;
  }
}


class Friends
{
  private $userId;
  private $friends;
  private $requestedFriend;
  private $requestingFriend;

  function __construct($userId) {
    $this->userId = $userId;
  }

  public function getFriends() {
    if (!isset($this->friends)) {
      throw new \Exception('Friends::friendsがまだセットされていない。');
    }
    return $this->idsToUserDatas($this->friends);
  }

  public function getRequestedFriend() {
    if (!isset($this->requestedFriend)) {
      throw new \Exception('Friends::requestedFriendがまだセットされていない。');
    }
    return $this->idsToUserDatas($this->requestedFriend);
  }

  public function getRequestingFriend() {
    if (!isset($this->requestingFriend)) {
      throw new \Exception('Friends::requestingFriendがまだセットされていない。');
    }
    return $this->idsToUserDatas($this->requestingFriend);
  }


  // 配列を更新。
  public function getFriendsData() {
    $this->friends          = [];
    $this->requestedFriend  = [];
    $this->requestingFriend = [];

    $friendsData = \func\user\getFriendsData($this->userId);
    if (is_array($friendsData)) {
      $this->friends          = $friendsData['friends'];
      $this->requestedFriend  = $friendsData['requested'];
      $this->requestingFriend = $friendsData['requesting'];
    }
  }


  private function idsToUserDatas($ids) {
    $userDatas = [];
    foreach ($ids as $id) {
      $name = \func\user\idToName($id);
      if ($name === false) {
        continue;
      }
      $userDatas[] = new UserData($id, $name);
    }
    return $userDatas;
  }


  private function getData($targetId, $column) {
    $pdo = new MyPDO();
    $sql = "SELECT `".$column."` FROM `user` WHERE `id` = :id;";
    $stmt = $pdo->prepare($sql);
    if (!$stmt->execute(['id' => $targetId])) {
      throw new \RuntimeException('データベースの処理に失敗した。');
    }

    $oldStr = $stmt->fetchColumn();
    if (!is_string($oldStr) || $oldStr === "") {
      return null;
    }
    $data = explode(',', substr($oldStr, 0, -1));
    return $data;
  }


  private function addData($targetId, $column, $addId) {
    try {
      $old = $this->getData($targetId, $column);
    } catch (\Exception $e) {
      return false;
    }

    if (is_array($old) && in_array($addId, $old, true)) {
      return true;
    }

    $pdo = new MyPDO();
    $sql = "UPDATE `user` ";
    $sql .= "SET `$column`= CONCAT(IFNULL(`$column`, ''), :addId, ',') ";
    $sql .= "WHERE `id` = :targetId;";

    $stmt = $pdo->prepare($sql);
    $parameter = [
      'addId'    => $addId,
      'targetId' => $targetId,
    ];
    return $stmt->execute($parameter);
  }


  private function makeChatFile($targetId, $addId) {
    $dir = DATA_DIR."/user/{$targetId}";
    $chat_dir = $dir."/chat";
    $private_chat_dir = $chat_dir."/private";
    $chat_file = $private_chat_dir."/{$addId}.json";
    if (!file_exists($dir) && !mkdir($dir)) {
      return false;
    }
    if (!file_exists($chat_dir) && !mkdir($chat_dir)) {
      return false;
    }
    if (!file_exists($private_chat_dir) && !mkdir($private_chat_dir)) {
      return false;
    }
    if (!file_exists($chat_file)) {
      $targetName = \func\user\idToName($targetId);
      $addName = \func\user\idToName($addId);

      $data = (object)[
        'info' => (object)[
          'me' => (object)[
            'name' => $targetName,
            'id' => $targetId
          ],
          'companion' => (object)[
            'name' => $addName,
            'id' => $addId
          ],
          'last_posted_date' => '1000-01-01 00:00:00',
          'cnt' => 0,
          'last_read' => 0
        ],
        'messages' => []
      ];
      $json = json_encode($data);

      $iv_size = openssl_cipher_iv_length(OPENSSL_METHOD);
      $iv = openssl_random_pseudo_bytes($iv_size);
      $encrypted = openssl_encrypt($json, OPENSSL_METHOD, OPENSSL_PASSWORD, OPENSSL_OPTIONS, $iv);
      if (!is_string($encrypted)) {
        return false;
      }
      $base64_iv = base64_encode($iv);
      $base64_encrypted = base64_encode($encrypted);
      $encryptedData = $base64_iv."\n".$base64_encrypted;

      $result = file_put_contents($chat_file, $encryptedData, LOCK_EX);
      if ($result === false) {
        return false;
      }
    }
    return true;
  }


  private function deleteData($targetId, $column, $deleteId) {
    try {
      $oldValues = $this->getData($targetId, $column);
    } catch (\Exception $e) {
      return false;
    }
    // すでに要素がないときには削除する必要がない。
    if (!is_array($oldValues) || count($oldValues) === 0) {
      return true;
    }

    $newValues = array_diff($oldValues, [$deleteId]);

    if (count($newValues) === 0) {
      $newValue = null;
    } else {
      $newValue = join(',', $newValues).',';
    }

    $pdo = new MyPDO();
    $sql = "UPDATE `user` SET `$column` = :newValue WHERE `id` = :targetId;";
    $stmt = $pdo->prepare($sql);
    $parameters = [
      'newValue' => $newValue,
      'targetId' => $targetId,
    ];
    return $stmt->execute($parameters);
  }

  // 申請
  public function requestFriend($name) {
    if (!is_string($name) || preg_match('/^[\s　]*$/', $name)) {
      throw new myexception\InputException('入力されたユーザー名の値が不適切。($name:'.$name.')');
    }
    $id = \func\user\nameToId($name);
    if ($id === $this->userId) {
      throw new myexception\OperationException('自分自信に対して申請している。');
    }
    if (is_null($id)) {
      throw new myexception\OperationException('存在しないユーザーに対して申請している。');
    }
    if ($id === false) {
      return false;
    }

    $myFriends = $this->getData($this->userId, 'friends');
    if (in_array($id, $myFriends, true)) {
      throw new myexception\OperationException('すでにfriendsに登録されているユーザーに対して申請している。');
    }

    // 相手の friends にすでにある場合
    $data = $this->getData($id, "friends");
    if ($data === false) {
      return false;
    }
    if (is_array($data) && in_array($this->userId, $data, true)) {
      if (!$this->addData($this->userId, "friends", $id)) {
        return false;
      }
      $this->makeChatFile($this->userId, $id);
      return true;
    }

    // 相手から申請されている場合
    $data = $this->getData($id, "requesting_friends");
    if ($data === false) {
      return false;
    }
    if (is_array($data) && in_array($this->userId, $data, true)) {
      if (!$this->addData($this->userId, "friends", $id)) {
        return false;
      }
      $this->makeChatFile($this->userId, $id);
      if (!$this->addData($id, "friends", $this->userId)) {
        return false;
      }
      $this->makeChatFile($id, $this->userId);
      if (!$this->deleteData($id, "requesting_friends", $this->userId)) {
        return false;
      }

      $data = $this->getData($this->userId, "requested_friends");
      if ($data === false) {
        return false;
      }
      if (is_array($data) && in_array($id, $data, true)) {
        if (!$this->deleteData($this->userId, "requested_friends", $id)) {
          return false;
        }
      }
      return true;
    }

    // 普通
    if (!$this->addData($id, "requested_friends", $this->userId)) {
      return false;
    }
    if (!$this->addData($this->userId, "requesting_friends", $id)) {
      return false;
    }
    return true;
  }


  // 申請取り消し
  public function cancelRequest($id) {
    if (!is_string($id) || preg_match('/^[\s　]*$/', $id)) {
      throw new myexception\InputException('入力されたユーザーIDの値が不適切。($id:'.$id.')');
    }
    if (!$this->deleteData($id, "requested_friends", $this->userId)) {
      return false;
    }
    if (!$this->deleteData($this->userId, "requesting_friends", $id)) {
      return false;
    }
    return true;
  }


  // 承認
  public function approval($id) {
    if (!is_string($id) || preg_match('/^[\s　]*$/', $id)) {
      throw new myexception\InputException('入力されたユーザーIDの値が不適切。($id:'.$id.')');
    }

    if (!$this->deleteData($id, "requesting_friends", $this->userId)) {
      return false;
    }
    if (!$this->deleteData($this->userId, "requested_friends", $id)) {
      return false;
    }
    if (!$this->addData($id, "friends", $this->userId)) {
      return false;
    }
    $this->makeChatFile($id, $this->userId);
    if (!$this->addData($this->userId, "friends", $id)) {
      return false;
    }
    $this->makeChatFile($this->userId, $id);
    return true;
  }

  // 申請を拒否
  // TODO: 申請してきた側も変更すべき??
  public function rejection($id) {
    if (!is_string($id) || preg_match('/^[\s　]*$/', $id)) {
      throw new myexception\InputException('入力されたユーザーIDの値が不適切。($id:'.$id.')');
    }
    if (!$this->deleteData($this->userId, "requested_friends", $id)) {
      return false;
    }
    return true;
  }


  // 友達削除
  public function deleteFriend($id) {
    if (!is_string($id) || preg_match('/^[\s　]*$/', $id)) {
      throw new myexception\InputException('入力されたユーザーIDの値が不適切。($id:'.$id.')');
    }
    if (!$this->deleteData($this->userId,"friends", $id)) {
      return false;
    }
    return true;
  }


  // 新規チャット作成
  private function makeChat() {

  }
}
