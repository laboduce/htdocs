<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Identification.php');

abstract class Controller
{
  private $idAction;
  // ログイン不要の場合は $destination に null を設定。
  function __construct($page=null, $destination='/user/login/') {
    if (is_null($page)) {
      $page = $_SERVER["REQUEST_URI"];
    }
    $this->idAction = new Identification($page);
    if (!is_null($destination)) {
      $this->checkLogged($destination);
    }
  }

  protected function getUserId() {
    $userData = $this->idAction->getUserData();
    return $userData['id'];
  }

  protected function getUserName() {
    $userData = $this->idAction->getUserData();
    return $userData['name'];
  }

  protected function getHistory($i) {
    return $this->idAction->getHistory($i);
  }


  private function checkLogged($destination){
    if ($this->idAction->getHasLogged()) {
      return;
    }
    if (is_string($destination)) {
      header('Location:'.$destination);
      exit();
    }
  }
}
