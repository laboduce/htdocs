<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/MyPDO.php');
require_once(CLASS_DIR.'/Exceptions.php');
define('HISTORY_SIZE', 30);
define('TIME_OUT', 3600);

class Identification
{
  private $isAuthenticated;
  private $hasLogged;
  private $userData;

  function __construct($nowPage) {
    session_start();
    $this->isAuthenticated = false;
    $this->hasLogged = false;
    $this->checkSession();
    $this->addHistory($nowPage);
  }


  private function addHistory($page) {
    if (!isset($_SESSION['history'])) {
      $_SESSION['history'] = [];
    }

    $cnt = count($_SESSION['history']);
    if ($cnt <= 2 || $page !== $_SESSION['history'][$cnt - 1] || $page !== $_SESSION['history'][$cnt - 2]) {
      $_SESSION['history'][] = $page;
      $cnt++;
    }

    while ($cnt-- > HISTORY_SIZE) {
      array_shift($_SESSION['history']);
    }
  }

  public function deleteHistory() {
    array_pop($_SESSION['history']);
  }

  public function getHistory($i) {
    if ($i < 1 || $i > count($_SESSION['history'])) {
      return null;
    } else {
      return $_SESSION['history'][count($_SESSION['history']) - $i];
    }
  }

  private function checkSession() {
    if (isset($_SESSION['hasLogged']) && $_SESSION['hasLogged']) {
      if ($this->checkTimeOut()) {
        $this->isAuthenticated = true;
        $this->hasLogged = true;
        $this->userData['id'] = $_SESSION['id'];
        $this->userData['name'] = $_SESSION['name'];
        return;
      }
    }

    unset($_SESSION['last']);
    $_SESSION['hasLogged'] = false;
    $_SESSION['id'] = '-1';
    $_SESSION['name'] = 'ゲスト';
    $this->userData['id'] = '-1';
    $this->userData['name'] = 'ゲスト';
    return;
  }

  private function checkTimeOut() {
    $now = time();
    if (!isset($_SESSION['last'])) {
      $_SESSION['last'] = $now;
      return true;// 初めてのログイン

    } else {
      $last = $_SESSION['last'];
      $_SESSION['last'] = $now;
      return ($now - $last < TIME_OUT);
    }
  }

  public function deleteSession() {
    $_SESSION = array();
    if (isset($_COOKIE['PHPSESSID'])) {
      setcookie("PHPSESSID", '', time() - 1800, '/');
    }
    session_destroy();
  }

  public function getHasLogged() {
    return $this->hasLogged;
  }

  public function getUserData() {
    $userData['id'] = $_SESSION['id'];
    $userData['name'] = $_SESSION['name'];
    return $userData;
  }


  public function checkPW($name, $pw) {
    $pdo = new MyPDO();
    $sql = "SELECT `id`, `name`, `pw` FROM `user` WHERE `name` = :name";
    $stmt = $pdo->prepare($sql);
    if (!$stmt->execute(['name' => $name])) {
      return false;
    }

    // ユーザー名を被らないようにするならばPDOStatement::fetch()でよい
    foreach ($stmt->fetchAll() as $row) {
      if (!password_verify($pw, $row['pw'])) {
        continue;
      }
      if (!$row['id']) {
        $error['db_error'][] = "id";
      }
      if (!$row['name']) {
        $error['db_error'][] = "name";
      }
      if (is_null($error)) {
        $this->userData['id'] = $row['id'];
        $this->userData['name'] = $row['name'];

        $this->isAuthenticated = true;
        return true;
      }
    }
    return false;
  }




  public function login($destination) {
    if (!$this->isAuthenticated) {
      return false;
    }

    $_SESSION['id'] = $this->userData['id'];
    $_SESSION['name'] = $this->userData['name'];
    $_SESSION['hasLogged'] = true;
    $this->hasLogged = true;
    header("Location:".$destination);
    exit();
  }


  public function logout($destination) {
    $this->isAuthenticated = false;
    $this->hasLogged = false;
    $_SESSION['hasLogged'] = false;
    $_SESSION['id'] = '-1';
    $_SESSION['name'] = 'ゲスト';
    header("Location:".$destination);
    exit();
  }


  public function signUp($data) {
    $pdo = new MyPDO();
    $sql = "SELECT EXISTS (SELECT * FROM `user` WHERE `name` = :name);";
    $stmt = $pdo->prepare($sql);
    if (!$stmt->execute(['name' => $data['name']])) {
      throw new \RuntimeException('ユーザー名がすでに使われているかチェックしようとしたがデータベースの処理に失敗した。');
    }
    if ($stmt->fetchColumn() !== '0') {
      throw new myexception\DBException('登録しようとしたユーザー名がすでに登録されている。', 1001);
    }

    do {
      $id = uniqid(mt_rand());
      $dir = DATA_DIR."/user/{$id}";
    } while (file_exists($dir));
    $data['id'] = $id;

    $hash = password_hash($data['pw'], PASSWORD_BCRYPT);
    $data['pw'] = $hash;

    $chat_dir = $dir."/chat";
    $private_chat_dir = $chat_dir."/private";
    $img_dir = $dir."/images";
    $group_chat_file = $chat_dir."/group.json";


    mkdir($dir);
    mkdir($chat_dir);
    mkdir($private_chat_dir);
    mkdir($img_dir);
    touch($group_chat_file);

    $sql  = "INSERT INTO `user` ";
    $sql .=        "(`id`, `name`, `pw`, `email`) ";
    $sql .= "VALUES (:id,  :name,  :pw,  :email );";
    $stmt = $pdo->prepare($sql);
    if (!$stmt->execute($data)) {
      throw new myexception\DBException('データベースに登録失敗。');
    }
    return true;
  }


}
