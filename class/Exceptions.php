<?php
namespace myexception;

/**
 * 入力値に不正がある場合
 *'入力された___の値が不適切。($___:'.$___.')'
 * $code
 * 1000: ファイルが送信されなかった。
 * 1001: 入力されたファイルが大きすぎる。
 * 1002: ファイルの形式が不適切。
 *
 *
 */
class InputException extends \Exception {
  function __construct($message, $code=0) {
    parent::__construct($message, $code);
  }
}

/**
 * データベース系に例外がある場合
 * $code
 * 1001: 登録しようとしたユーザー名がすでに登録されている。
 */
class DBException extends \Exception {
  function __construct($message, $code=0) {
    parent::__construct($message, $code);
  }
}

/**
 * 不正な権限でのアクセス
 * EX) 掲示板の管理者でないのにその掲示板を削除しようとした
 * $code
 */
class AuthorityException extends \Exception {
  function __construct($message, $code=0) {
    parent::__construct($message, $code);
  }
}


/**
 * 操作をしようとしたができなかった。
 * EX) すでに招待されているユーザーを招待しようとした
 * $code
 *
 */
class OperationException extends \Exception {
  function __construct($message, $code=0) {
    parent::__construct($message, $code);
  }
}
