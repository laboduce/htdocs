<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(DB_PROPERTIES_PATH);


class MyPDO extends pdo  {

  function __construct() {
    parent::__construct(PDO_DSN, DATABASE_USER, DATABASE_PASSWORD);
  }


}
