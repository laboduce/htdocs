<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Exceptions.php');
require_once(CLASS_DIR.'/Message.php');
require_once(FUNCTION_DIR.'/encrypt.php');
require_once(CLASS_DIR.'/LikeListFile.php');

abstract class Chat
{
  protected $userId;
  protected $roomId;
  private $messages;

  function __construct($userId) {
    $this->userId = $userId;


    $dir = DATA_DIR.'/user/'.$userId;
    $chatDir = $dir.'/chat';
    $filename = $chatDir.'/group.json';
    if (!file_exists($filename)) {
      if (!file_exists($dir)) {
        mkdir($dir);
      }
      if (!file_exists($chatDir)) {
        mkdir($chatDir);
      }
      $data = (object)['participate' => [], 'invited' => []];
      try {
        \func\encrypt\saveEncryptedFile($filename, $data);
      } catch (\Exception $e) {

      }
    }
  }


  protected abstract function addMessage($messageData);

  protected abstract function addLike($messageId);

  protected abstract function validateRoomId($roomId);

  protected abstract function loadMembersAndMessages();



  public function setRoomId($roomId) {
    $errorMessage = $this->validateRoomId($roomId);
    if (is_string($errorMessage)) {
      throw new myexception\OperationException($errorMessage);
    }
    $this->roomId = $roomId;
  }


  public function postMessage($message) {
    if (!is_string($message) || preg_match('/^[\s　]*$/', $message)) {
      throw new myexception\InputException('入力された本文の値が不適切。($message:'.$message.')');
    }

    $messageData = $this->makeMessageData($message);
    $this->addMessage($messageData);
  }


  public function replyMessage($message, $messageId) {
    if (!is_string($message) || preg_match('/^[\s　]*$/', $message)) {
      throw new myexception\InputException('入力された本文の値が不適切。($message:'.$message.')');
    }

    if (!is_string($messageId) || !preg_match('/^[1-9]\d*$/', $messageId)) {
      throw new myexception\InputException('入力された返信先のメッセージIDが不適切。($messageId:'.$messageId.')');
    }

    if (!$this->messageExists($messageId)) {
      throw myexception\OperationException('存在しないメッセージに対して返信しようとしている。');
    }

    $messageData = $this->makeMessageData($message, $messageId);
    $this->addMessage($messageData);
  }


  public function pressLike($messageId) {
    if (!is_string($messageId) || !preg_match('/^[1-9]\d*$/', $messageId)) {
      throw new myexception\InputException('いいね先のメッセージIDが不適切。($messageId:'.$messageId.')');
    }

    $likeList = new LikeListFile($this->userId, $this->roomId);
    $likeList->pressLike($messageId);
    $likeList->saveLikeListFile();

    $this->addLike($messageId);
  }


  public function getMessages($begine, $range) {
    if (!is_array($this->messages)) {
      throw new \Exception('$messagesがまだセットされていない。');
    }
    $messages = array_slice(array_reverse($this->messages), $begine-1, $range);
    return $messages;
  }


  public function getMessageCount() {
    if (!is_array($this->messages)) {
      throw new \Exception('$messagesがまだセットされていない。');
    }
    return count($this->messages);
  }


  public function setMessages() {
    $mm = $this->loadMembersAndMessages();
    $members = $mm['members'];
    $allMessages = $mm['messages'];

    $messages = [];
    foreach ($allMessages as $message) {
      $messageId = $message->message_id;
      $postedDate = $message->posted_date;
      $body = $message->body;
      $like = $message->like;

      if (isset($members->{$message->from_id})) {
        $fromId = $message->from_id;
        $name = $members->$fromId;
      } else {
        $fromId = -1;
        $name = "退会済み";
      }

      if (isset($message->to_id)){
        $toId = $message->to_id;
      } else {
        $toId = null;
      }

      if (isset($message->reply)){
        $reply = $message->reply;
      } else {
        $reply = null;
      }
      $temp = new Message($messageId, $fromId, $toId, $postedDate, $name, $reply, $body, $like);
      if ($temp->isSafe) {
        unset($temp->isSafe);
        $messages[] = $temp;
      }
    }
    $this->messages = $messages;
  }



  protected function getNow() {
    return date('Y-m-d H:i:s');
  }

  private function makeMessageData($message, $messageId=null) {
    $messageData = [
      "body" => $message,
      "posted_date" => $this->getNow(),
      "from_id" => $this->userId,
      "like" => 0,
      "reply" => [],
    ];
    if (!is_null($messageId)) {
      $messageData['to_id'] = $messageId;
    }
    return $messageData;
  }



}
