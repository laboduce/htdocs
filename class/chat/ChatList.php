<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Friends.php');
require_once(FUNCTION_DIR.'/encrypt.php');
require_once(CLASS_DIR.'/chat/GroupChat.php');


class ChatList
{
  private $userId;
  private $friends;
  private $participatingGroups;
  private $invitedGroups;

  function __construct($userId) {
    $this->userId = $userId;

    $myFriend = new Friends($userId);
    $myFriend->getFriendsData();
    $this->friends = $myFriend->getFriends();

    $this->setChatList();
  }

  public function getFriends() {
    return $this->friends;
  }

  public function getParticipatingGroups() {
    return $this->participatingGroups;
  }

  public function getInvitedGroups() {
    return $this->invitedGroups;
  }

  public function withdrawGroup($groupId) {
    $groupChat = new GroupChat($this->userId);
    $groupChat->withdrawGroup($groupId);
    $this->setChatList();
  }

  public function joinGroup($groupId) {
    $groupChat = new GroupChat($this->userId);
    $groupChat->joinGroup($groupId);
    $this->setChatList();
  }

  public function createGroup($name, $description) {
    $groupChat = new GroupChat($this->userId);
    $groupId = $groupChat->createGroup($name, $description);
    $this->setChatList();
    return $groupId;
  }


  private function setChatList() {
    $dir = DATA_DIR.'/user/'.$this->userId;
    $chatDir = $dir.'/chat';
    $filename = $chatDir.'/group.json';

    try {
      $data = \func\encrypt\loadEncryptedFile($filename);
      $this->participatingGroups = $data->participate;
      $this->invitedGroups = $data->invited;

    } catch (\Exception $e) {
      if (!file_exists($dir)) {
        mkdir($dir);
      }
      if (!file_exists($chatDir)) {
        mkdir($chatDir);
      }
      $data = (object)['participate' => [], 'invited' => []];
      try {
        \func\encrypt\saveEncryptedFile($filename, $data);
      } catch (\Exception $e) {

      }
      $this->participatingGroups = [];
      $this->invitedGroups = [];
    }
  }
}
