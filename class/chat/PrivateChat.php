<?php
define('MAX_SAVE_PRIVATE_MESSAGES', 300); // 個人チャットで保存するメッセージ数の上限
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/chat/Chat.php');
require_once(CLASS_DIR.'/ISocialConversation.php');
require_once(CLASS_DIR.'/Exceptions.php');
require_once(FUNCTION_DIR.'/encrypt.php');
require_once(CLASS_DIR.'/Friends.php');
require_once(CLASS_DIR.'/MyPDO.php');
require_once(FUNCTION_DIR.'/user.php');


class PrivateChat extends Chat implements ISocialConversation
{


  public function getRoomInfo() {
    try {
      $data = $this->loadPrivateChatFile($this->userId, $this->roomId);
      $roomInfo = $data->info;

    } catch (\Exception $e) {
      $roomInfo = new stdClass();
      $roomInfo->companion = new stdClass();
      $roomInfo->companion->id = $this->roomId;

      $name = \func\user\idToName($this->roomId);
      if ($name !== false) {
        $roomInfo->companion->name = $name;
      } else {
        $roomInfo->companion->name = null;
      }
    }
    return $roomInfo;
  }



  protected function addMessage($messageData) {
    if (is_null($this->roomId)) {
      throw new \Exception('$companionがセットされていない。');
    }
    $myData = $this->loadPrivateChatFile($this->userId, $this->roomId);
    $companionsData = $this->loadPrivateChatFile($this->roomId, $this->userId);

    $myData->info->cnt++;
    $companionsData->info->cnt++;
    $myData->info->last_posted_date = $messageData['posted_date'];
    $companionsData->info->last_posted_date = $messageData['posted_date'];

    $messageData['message_id'] = $myData->info->cnt;
    $myData->messages[] = (object)$messageData;
    $messageData['message_id'] = $companionsData->info->cnt;
    $companionsData->messages[] = (object)$messageData;

    $modifiedMessages = array_slice($myData->messages, -MAX_SAVE_PRIVATE_MESSAGES);
    if ($modifiedMessages) {
      $myData->messages = $modifiedMessages;
    }
    $modifiedMessages = array_slice($companionsData->messages, -MAX_SAVE_PRIVATE_MESSAGES);
    if ($modifiedMessages) {
      $companionsData->messages = $modifiedMessages;
    }
    unset($modifiedMessages);
    // // 既読処理
    // $cnt = $myData->info->cnt;
    // $last_read = $this->data->info->last_read;
    // if ($last_read === $cnt - 1) {
    //   $this->data->info->last_read = $cnt;
    // }

    $this->savePrivateChatFile($this->userId, $this->roomId, $myData);
    $this->savePrivateChatFile($this->roomId, $this->userId, $companionsData);
  }

  protected function addLike($messageId) {
    if (is_null($this->roomId)) {
      throw new \Exception('$companionがセットされていない。');
    }
    $messageId = (int)$messageId;
    $myData = $this->loadPrivateChatFile($this->userId, $this->roomId);
    $companionsData = $this->loadPrivateChatFile($this->roomId, $this->userId);

    $indexs = [-1, -1];
    foreach ([$myData, $companionsData] as $k => $data) {
      $lastMessage = $data->messages[count($data->messages)-1];
      if ($lastMessage->message_id < $messageId) {
        throw new myexception\OperationException('存在しないメッセージにいいねしようとした。');
      }
      foreach ($data->messages as $key => $message) {
        if ($message->message_id === $messageId) {
          $indexs[$k] = $key;
          break;
        }
      }
    }

    if ($indexs[0] !== -1) {
      $myData->messages[$indexs[0]]->like++;
      $this->savePrivateChatFile($this->userId, $this->roomId, $myData);
    }
    if ($indexs[1] !== -1) {
      $companionsData->messages[$indexs[1]]->like++;
      $this->savePrivateChatFile($this->roomId, $this->userId, $companionsData);
    }
  }

  // 友達かのチェック
  protected function validateRoomId($companion) {
    if (!is_string($companion) || preg_match('/^[\s　]*$/', $companion)) {
      return '入力された相手のユーザーIDの値が不適切。($companion:'.$companion.')';
    }
    $myFriend = new Friends($this->userId);
    $myFriend->getFriendsData();
    $friends  = $myFriend->getFriends();
    foreach($friends as $friend) {
      if ($companion === $friend->id) {
        $resultOfTouch = $this->touchPrivateChatFile($this->userId, $companion);
        if (is_string($resultOfTouch)) {
          return $result;
        }
        $resultOfTouch = $this->touchPrivateChatFile($companion, $this->userId);
        if (is_string($resultOfTouch)) {
          return $result;
        }
        return;
      }
    }
    return 'このユーザー('.$companion.')は友達ではない。';
  }

  protected function loadMembersAndMessages() {
    try {
      $data = $this->loadPrivateChatFile($this->userId, $this->roomId);
      $messages = $data->messages;
      $members = (object)[
        $data->info->me->id => $data->info->me->name,
        $data->info->companion->id => $data->info->companion->name,
      ];
    } catch (\Exception $e) {
      $messages = [];
      $members = (object)[];
    }
    return ['messages' => $messages, 'members' => $members];
  }





  private function loadPrivateChatFile($me, $companion) {
    $filename = DATA_DIR.'/user/'.$me.'/chat/private/'.$companion.'.json';
    return \func\encrypt\loadEncryptedFile($filename);
  }

  private function savePrivateChatFile($me, $companion, $data) {
    $filename = DATA_DIR.'/user/'.$me.'/chat/private/'.$companion.'.json';
    \func\encrypt\saveEncryptedFile($filename, $data);
  }


  private function touchPrivateChatFile($me, $companion) {
    $dir = DATA_DIR.'/user/'.$me;
    $chatDir = $dir.'/chat';
    $privateChatDir = $chatDir.'/private';
    $filename = $privateChatDir.'/'.$companion.'.json';
    if (!file_exists($filename)) {
      if (!file_exists($dir)) {
        mkdir($dir);
      }
      if (!file_exists($chatDir)) {
        mkdir($chatDir);
      } if (!file_exists($privateChatDir)) {
        mkdir($privateChatDir);
      }
      $data = (object)[
        'info' => (object)[
          'companion' => (object)[
            'id' => $companion,
            'name' => \func\user\idToName($companion)
          ],
          'me' =>  (object)[
            'id' => $me,
            'name' => \func\user\idToName($me)
          ],
          "last_posted_date" => "1000-01-01 00:00:00",
          "cnt" => 0,
          "last_read" => 0,
        ],
        'messages' => []
      ];
      try {
        \func\encrypt\saveEncryptedFile($filename, $data);
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }

    return true;
  }


}
