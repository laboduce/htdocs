<?php
define('MAX_SAVE_GROUP_MESSAGES', 500);   // グループチャットで保存するメッセージ数の上限
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/chat/Chat.php');
require_once(CLASS_DIR.'/ISocialConversation.php');
require_once(CLASS_DIR.'/Exceptions.php');
require_once(CLASS_DIR.'/MyPDO.php');
require_once(FUNCTION_DIR.'/encrypt.php');
require_once(FUNCTION_DIR.'/user.php');



class GroupChat extends Chat implements ISocialConversation
{

  public function inviteMember($userName, $message) {
    if (is_null($this->roomId)) {
      throw new \Exception('$groupIdがセットされていない。');
    }
    if (!is_string($userName) || preg_match('/^[\s　]*$/', $userName)) {
      throw new myexception\InputException('ユーザー名が不適切。($userName:'.$userName.')');
    }
    $data = $this->loadGroupFile();
    $info = $data->info;

    $id = \func\user\nameToId($userName);
    if ($id === false){
      throw new myexception\DBException('ユーザーIDを取得しようとしたがデータベースでエラー。');
    }
    if (is_null($id)) {
      throw new myexception\OperationException('存在しないユーザーを招待しようとしている。');
    }
    if (isset($info->member->$id)) {
      throw new myexception\OperationException('すでに参加済のユーザーを招待しようとしている。($userName:'.$userName.')');
    }
    if (isset($info->inviting->$id)) {
      throw new myexception\OperationException('すでに招待済のユーザーを招待しようとしている。($userName:'.$userName.')');
    }
    $info->inviting->$id = $userName;


    $groupData = (object)[
      'group_id' => $info->group_id,
      "group_name" => $info->group_name,
      "inviter" => (object)["id" => $this->userId, "name" => $info->member->{$this->userId}],
      "invited_date" => parent::getNow()
    ];
    if (is_string($message) && !preg_match('/^[\s　]*$/', $message)) {
      $groupData->message = $message;
    }

    try {
      $personalData = $this->loadPersonalGroupFile($id);
      if (isset($personalData->invited) && is_array($personalData->invited)) {
        $personalData->invited[] = $groupData;
      } else {
        $personalData->invited = [$groupData];
      }
    } catch (\RuntimeException $e) {
      $personalData = (object)['participate' => [], 'invited' => [$groupData]];
    }

    $this->savePersonalGroupFile($personalData, $id);
    $this->saveGroupFile($data);

  }

  public function getGroupInfo() {
    try {
      $data = $this->loadGroupFile();
      $info = $data->info;
    } catch (\Exception $e) {
      $info = (object)[]; // TODO:
    }

    return $data->info;
  }

  public function createGroup($name, $description) {
    if (!is_string($name) || preg_match('/^[\s　]*$/', $name)) {
      throw new myexception\InputException('入力された新しい掲示板名が不適切。($name:'.$name.')');
    }
    if (!is_string($description) || preg_match('/^[\s　]*$/', $description)) {
      $description = null;
    }
    $userName = \func\user\idToName($this->userId);
    if ($userName === false){
      throw new myexception\DBException('ユーザー名を取得しようとしたがデータベースでエラー。');
    }

    $now = parent::getNow();
    do {
      $groupId = uniqid(mt_rand());
      $newGroupData = (object)[
        "info" => (object)[
          "group_name" => $name,
          "group_id" => $groupId,
          "master" => (object)["id" => $this->userId, "name" => $userName],
          "member" => (object) [$this->userId => $userName],
          "inviting" => (object)[],
          "created_date" => $now,
          "last_posted_date" => $now,
          "cnt" => 0,
          "description" => $description
        ],
        "messages" => []
      ];
      $filename = DATA_DIR.'/group_chat/'.$groupId.'.json';
    } while (file_exists($filename));
    \func\encrypt\saveEncryptedFile($filename, $newGroupData);

    $data = $this->loadPersonalGroupFile($this->userId);
    if (!$data) {
      $data = (object)[
        'participate' => []
      ];
    }
    $newGroupData->info->last_read = 0;
    unset($newGroupData->info->inviting);
    $data->participate[] = $newGroupData->info;
    $this->savePersonalGroupFile($data, $this->userId);
    return $groupId;
  }

  public function deleteGroup() {
    if (is_null($this->roomId)) {
      throw new \Exception('$groupIdがセットされていない。');
    }
    $data = $this->loadGroupFile();
    $info = $data->info;
    if ($info->master->id !== $this->userId) {
      throw new myexception\AuthorityException('管理者以外のユーザーがグループチャットを削除しようとしている。');
    }

    // メンバー
    foreach ($info->member as $id => $name) {
      try {
        $personalData = $this->loadPersonalGroupFile($id);
      } catch (\RuntimeException $e) {
        continue;
      }
      foreach ($personalData->participate as $key => $group) {
        if ($group->group_id === $this->roomId) {
          array_splice($personalData->participate, $key, 1);
          try {
            $this->savePersonalGroupFile($personalData, $id);
            break;
          } catch (\RuntimeException $e) {
            break;
          }
        }
      }
    }

    // 招待中
    if (isset($info->inviting)) {
      foreach ($info->inviting as $id => $name) {
        try {
          $personalData = $this->loadPersonalGroupFile($id);
        } catch (\RuntimeException $e) {
          continue;
        }
        foreach ($personalData->invited as $key => $group) {
          if ($group->group_id === $this->roomId) {
            array_splice($personalData->invited, $key, 1);
            try {
              $this->savePersonalGroupFile($personalData, $id);
              break;
            } catch (\RuntimeException $e) {
              break;
            }
          }
        }
      }
    }
    // 全体
    $filename = DATA_DIR.'/group_chat/'.$this->roomId.'.json';
    unlink($filename);
  }

  public function joinGroup($groupId) {
    if (!is_string($groupId)) {
      throw new myexception\InputException('グループIDが不適切。($groupId:'.$groupId.')');
    }
    $filename = DATA_DIR.'/group_chat/'.$groupId.'.json';
    $data = \func\encrypt\loadEncryptedFile($filename);
    // 招待されているか
    $info = $data->info;
    if (!isset($info->inviting->{$this->userId})) {
      throw new myexception\OperationException('招待されていないグループに参加しようとしている。');
    }

    $info->member->{$this->userId} = $info->inviting->{$this->userId};
    unset($info->inviting->{$this->userId});
    $filename = DATA_DIR.'/group_chat/'.$groupId.'.json';
    \func\encrypt\saveEncryptedFile($filename, $data);

    // 個人ファイルの更新
    $personalData = $this->loadPersonalGroupFile($this->userId);

    foreach ($personalData->invited as $key => $group) {
      if ($groupId === $group->group_id) {
        array_splice($personalData->invited, $key, 1);
        unset($info->inviting);
        $personalData->participate[] = $info;
        $this->savePersonalGroupFile($personalData, $this->userId);
        break;
      }
    }

    foreach ($info->member as $id => $name) {
      if ($id === $this->userId) {
        continue;
      }
      try {
        $personalData = $this->loadPersonalGroupFile($id);
      } catch (\RuntimeException $e) {
        continue;
      }
      foreach ($personalData->participate as $group) {
        if ($group->group_id === $groupId) {
          $group->member = $info->member;
          break;
        }
      }
      try {
        $this->savePersonalGroupFile($personalData, $id);
      } catch (\RuntimeException $e) {
        continue;
      }
    }
  }

  public function withdrawGroup($groupId) {
    if (!is_string($groupId)) {
      throw new myexception\InputException('グループIDが不適切。($groupId:'.$groupId.')');
    }
    $filename = DATA_DIR.'/group_chat/'.$groupId.'.json';
    $data = \func\encrypt\loadEncryptedFile($filename);
    if ($this->userId === $data->info->master->id) {
      throw new myexception\AuthorityException("管理者権限のユーザーは退会することはできない。");
    }

    foreach ($data->info->member as $id => $name) {
      try {
        $personalData = $this->loadPersonalGroupFile($id);
      } catch (\RuntimeException $e) {
        continue;
      }

      if ($id === $this->userId) {
        foreach ($personalData->participate as $key => $group) {
          if ($groupId === $group->group_id) {
            array_splice($personalData->participate, $key, 1);
            break;
          }
        }

      } else {
        foreach ($personalData->participate as $key => $group) {
          if ($groupId === $group->group_id) {
            unset($personalData->participate[$key]->member->{$this->userId});
            break;
          }
        }
      }
      try {
        $this->savePersonalGroupFile($personalData, $id);
      } catch (\RuntimeException $e) {
        continue;
      }
    }

    unset($data->info->member->{$this->userId});
    $filename = DATA_DIR.'/group_chat/'.$groupId.'.json';
    $data = \func\encrypt\saveEncryptedFile($filename, $data);
  }



  protected function addMessage($messageData) {
    if (is_null($this->roomId)) {
      throw new \Exception('$groupIdがセットされていない。');
    }
    $data = $this->loadGroupFile();
    $data->info->cnt++;
    $data->info->last_posted_date = $messageData['posted_date'];

    $messageData['message_id'] = $data->info->cnt;
    $data->messages[] = (object)$messageData;

    $modifiedMessages = array_slice($data->messages, -MAX_SAVE_GROUP_MESSAGES);
    if ($modifiedMessages) {
      $data->messages = $modifiedMessages;
    }
    unset($modifiedMessages);
    $this->saveGroupFile($data);

    $members = $data->info->member;
    $info = $data->info;
    foreach ($members as $memberId => $name) {
      try {
        $personalData = $this->loadPersonalGroupFile($memberId);
      } catch (\RuntimeException $e) {
        continue;
      }

      foreach ($personalData->participate as $group) {
        if ($group->group_id === $this->roomId) {
          $group->cnt = $info->cnt;
          $group->last_posted_date = $info->last_posted_date;
          // // 既読処理
          // if ($this->userId === $memberId) {
          //   if ($this->lastRead === $info->cnt - 1) {
          //     $this->lastRead = $info->cnt;
          //   }
          //   $group->last_read = $info->cnt;
          // }
          break;
        }
      }
      try {
        $this->savePersonalGroupFile($personalData, $memberId);
      } catch (\RuntimeException $e) {
        continue;
      }
    }
  }

  // TODO: 返信は新しいメッセージにつきやすいだろうから非効率？？
  protected function addLike($messageId) {
    if (is_null($this->roomId)) {
      throw new \Exception('$groupIdがセットされていない。');
    }
    $messageId = (int)$messageId;
    $data = $this->loadGroupFile();
    $lastMessage = $data->messages[count($data->messages)-1];
    if ($lastMessage->message_id < $messageId) {
      throw new myexception\OperationException('存在しないメッセージにいいねしようとした。');
    }
    foreach ($data->messages as $key => $message) {
      if ($message->message_id === $messageId) {
        $data->messages[$key]->like++;
        $this->saveGroupFile($data);
        break;
      }
    }
  }

  // 参加しているかのチェック
  protected function validateRoomId($groupId) {
    if (!is_string($groupId) || preg_match('/^[\s　]*$/', $groupId)) {
      return 'グループIDが不適切。($groupId:'.$groupId.')';
    }

    $filename = DATA_DIR.'/group_chat/'.$groupId.'.json';
    $data = \func\encrypt\loadEncryptedFile($filename);
    foreach ($data->info->member as $id => $name) {
      if ($this->userId === $id) {
        return;
      }
    }
    return 'グループ('.$groupId.')に参加していない。';
  }

  protected function loadMembersAndMessages() {
    try {
      $data = $this->loadGroupFile();
      $messages = $data->messages;
      $members = $data->info->member;
    } catch (\Exception $e) {
      $messages = [];
      $members = (object)[];
    }
    return ['messages' => $messages, 'members' => $members];
  }



  private function loadPersonalGroupFile($userId) {
    $filename = DATA_DIR.'/user/'.$userId.'/chat/group.json';
    return \func\encrypt\loadEncryptedFile($filename);
  }

  private function loadGroupFile() {
    if (is_null($this->roomId)) {
      throw new \Exception('$groupIdがセットされていない。');
    }
    $filename = DATA_DIR.'/group_chat/'.$this->roomId.'.json';
    return \func\encrypt\loadEncryptedFile($filename);
  }


  private function savePersonalGroupFile($data, $userId) {
    $dir = DATA_DIR.'/user/'.$userId;
    $chatDir = $dir.'/chat';
    $filename = $chatDir.'/group.json';
    if (!file_exists($chatDir)) {
      if (!file_exists($dir)) {
        mkdir($dir);
      }
      mkdir($chatDir);
    }
    \func\encrypt\saveEncryptedFile($filename, $data);
  }

  private function saveGroupFile($data) {
    if (is_null($this->roomId)) {
      throw new \Exception('$groupIdがセットされていない。');
    }
    $filename = DATA_DIR.'/group_chat/'.$this->roomId.'.json';
    \func\encrypt\saveEncryptedFile($filename, $data);
  }


}
