<?php



class DiscussionBoardList
{
  private $pdo;
  private $userId;
  private $boardIds;

  function __construct($userId) {
    $this->userId = $userId;

    try {
      $this->pdo =  new MyPDO();
    } catch (PDOException $e) {
      // TODO: データベース
    }
  }

  // 掲示板数
  public function getRoomCount($category, $isAbs) {
    $roomIds = $this->getBoardIdsOrderByCategory($category, $isAbs);
    if (is_array($roomIds)) {
      return count($roomIds);
    }

    $sql = "SELECT COUNT(*) FROM `discussion_board_master`";
    $stmt = $this->pdo->query($sql);
    if (!$stmt) {
      throw new \RuntimeException('掲示板数を取得しようとしたがデータベースの処理に失敗した。');
    }
    $cnt = $stmt->fetchColumn();
    return (int)$cnt;
  }


  // ここを簡単にするために、掲示板を作成・削除するごとにcategory_master.jsonをソートしている。
  public function getCategoryRank($cnt) {
    $filename = DATA_DIR."/discussion_board/category_master.json";
    $json = file_get_contents($filename);
    if (is_string($json)) {
      $categories = json_decode($json);
    } else {
      $categories = [];
    }
    return array_slice($categories, 0, $cnt);
  }


  public function getDiscussionBoards($begine, $range, $orderBy, $category, $isAbs) {
    $roomIds = $this->getBoardIdsOrderByCategory($category, $isAbs);
    // １つも一致するものがないときにはデータベースにアクセスする必要ない
    if ($roomIds === []) {
      return [];
    }

    $sql = $this->makeGetDiscussionBoardSQL($roomIds, $orderBy);
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue('begine', (int)$begine - 1, PDO::PARAM_INT);
    $stmt->bindValue('range',  (int)$range,  PDO::PARAM_INT);
    if (!$stmt->execute()) {
      throw new \RuntimeException('掲示板のリストを取得しようとしたがデータベースの処理に失敗した。');
    }

    $datas = $stmt->fetchAll();
    $result = [];
    foreach ($datas as $data) {
      $temp = new BoardData($data);
      if ($temp->isSafe) {
        unset($temp->isSafe);
        $result[] = $temp;
      }
    }
    return $result;
  }



  private function makeGetDiscussionBoardSQL($roomIds, $orderBy) {
    // WHERE句
    if (!is_array($roomIds)) {
      $where = "1 ";
    } else {
      $where = "`discussion_board_master`.`board_id` IN ('".join("', '", $roomIds)."') ";
    }


    // ORDER句
    $orders = ['views', 'created_date', 'cnt', 'last_posted_date'];
    if (in_array($orderBy, $orders, true)) {
      $order = "`".$this->roomId."`.`".$orderBy."` DESC ";
    } else {
      $order = "`views` DESC ";
    }


    $sql = "SELECT ";
    $masterRows = ["board_id", "board_name", "created_date", "last_posted_date", "master_id", "cnt", "description", "views", "category"];
    foreach ($masterRows as $row) {
      $sql .= "`discussion_board_master`.`".$row."`, ";
    }
    $sql .= "`user`.`name` ";
    $sql .= "FROM `discussion_board_master`, `user` ";
    $sql .= "WHERE ".$where;
    $sql .= "AND `discussion_board_master`.`master_id` = `user`.`id` ";
    $sql .= "ORDER BY ".$order;
    $sql .= "LIMIT :range OFFSET :begine;";
    return $sql;
  }





  // カテゴリで検索
  private function getBoardIdsOrderByCategory($category, $isAbs) {
    if (!is_string($category) || preg_match('/^[\s　]*$/', $category)) {
      // カテゴリ検索しない
      return true;
    }

    // カテゴリ名配列( $categories )の最適化
    $inputCategoryStr = mb_convert_kana($category, "rnasK");
    $inputCategoryArr = array_unique(preg_split("/[\s,、。]+/u", $inputCategoryStr));
    $inputCategoryArr = array_merge($inputCategoryArr);

    $duplication = [];
    foreach ($inputCategoryArr as $ii => $cate) {
      for ($i=0; $i < $ii; $i++) {
        $short = $inputCategoryArr[$i];
        $long  = $cate;
        if (strlen($long) < strlen($short)) {
          $temp  = $long;
          $long  = $short;
          $short = $temp;
        }
        if (!preg_match('/'.preg_quote($short, '/').'/', $long)) {
          continue;
        }
        if (!in_array($short, $duplication, true)) {
          $duplication[] = $short;
        }
      }
    }
    $optimizedInputCategories = array_diff($inputCategoryArr, $duplication);


    // AND 検索
    $filename = DATA_DIR."/discussion_board/categories.json";
    $json = file_get_contents($filename);
    if (!is_string($json)) {
      return [];
    }
    $roomIds = json_decode($json, true);
    if (!is_array($roomIds)) {
      return [];
    }


    $roomIdsEachCategories = [/*
      [あるカテゴリに属する掲示板のIDの配列], ...
    */];
    if ($isAbs) {
      // 完全一致の場合
      foreach ($optimizedInputCategories as $needleCategory) {
        if (!isset($roomIds[$needleCategory])) {
          return [];
        }
        $roomIdsEachCategories[] = $roomIds[$needleCategory];
      }


    } else {
      // abcd に対して bc もマッチする場合
      foreach ($optimizedInputCategories as $needleCategory) {
        $matchedCategories = array_filter($roomIds, function($category) use ($needleCategory) {
          return preg_match('/^.*'.preg_quote($needleCategory, '/').'.*$/', $category);
        }, ARRAY_FILTER_USE_KEY);

        if (count($matchedCategories) === 0) {
          return [];
        }

        $roomIdsEachCategories[] = array_unique(call_user_func_array('array_merge', $matchedCategories));
      }
    }


    if (count($roomIdsEachCategories) === 1) {
      return $roomIdsEachCategories[0];
    } else {
      return call_user_func_array('array_intersect', $roomIdsEachCategories);
    }
  }







}
