<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/ISocialConversation.php');
require_once(CLASS_DIR.'/Exceptions.php');
require_once(CLASS_DIR.'/MyPDO.php');
require_once(CLASS_DIR.'/LikeListFile.php');
require_once(CLASS_DIR.'/Message.php');


class BoardData
{
  public $board_id;
  public $board_name;
  public $created_date;
  public $last_posted_date;
  public $master_id;
  public $master_name;
  public $cnt;
  public $description;
  public $views;
  public $categories;

  public $isSafe;

  function __construct($data) {
    $board_id = $data['board_id'];
    $board_name = $data['board_name'];
    $created_date = $data['created_date'];
    $last_posted_date = $data['last_posted_date'];
    $master_id = $data['master_id'];
    $master_name = $data['name'];
    $cnt = $data['cnt'];
    $description = $data['description'];
    $views = $data['views'];
    $category = $data['category'];

    $this->isSafe = false;
    if (!is_string($board_id) || $board_id === "") {
      return;
    }
    $this->board_id = $board_id;

    if (!is_string($board_name) || $board_name === "") {
      return;
    }
    $this->board_name = $board_name;

    $this->isSafe = true;
    $created_date = strtotime($created_date);
    $last_posted_date = strtotime($last_posted_date);
    if ($created_date !== false) {
      $this->created_date = $created_date;
    }
    if ($last_posted_date !== false) {
      $this->last_posted_date = $last_posted_date;
    }

    if (is_string($master_id) && $master_id !== "") {
      $this->master_id = $master_id;
    }
    if (is_string($master_name) && $master_name !== "") {
      $this->master_name = $master_name;
    }

    if (preg_match('/^\d+$/', $cnt)) {
      $this->cnt = $cnt - 1;
    }

    if (preg_match('/^\d+$/', $views)) {
      $this->views = $views;
    }

    if (is_string($description) && $description !== "") {
      $this->description = preg_replace('/&#(13|xd);&#(10|xa);/i', "<br>\n", $description);
    } else {
      $this->description = "(なし)";
    }

    if (is_string($category)) {
      $this->categories = explode(",", $category);
    }

  }
}




class DiscussionBoard implements ISocialConversation
{
  private $pdo;

  private $userId;
  private $roomId;

  function __construct($userId) {
    $this->userId = $userId;

    try {
      $this->pdo =  new MyPDO();
    } catch (PDOException $e) {
      // TODO: データベース
    }
  }


  public function setRoomId($roomId) {
    if (!is_string($roomId)) {
      throw new myexception\InputException('入力された掲示板IDの値がstringでない。');
    }

    $sql = "SELECT EXISTS (SELECT * FROM `discussion_board_master` WHERE `board_id` = '".$roomId."');";
    $result = $this->pdo->query($sql);
    if (!($result)) {
      throw new \RuntimeException('掲示板の存在確認をしようとしたがデータベースの処理に失敗した。');
    }
    $data = $result->fetchall();
    if (count($data) !== 1 || $data[0][0] !== '1') {
      throw new myexception\OperationException('IDが'.$roomId.'の掲示板が存在しなかった。');
    }
    $this->roomId = $roomId;
  }


  public function getMessageCount() {
    $sql = "SELECT COUNT(*) FROM `".$this->roomId."`";
    $stmt = $this->pdo->query($sql);
    if (!$stmt) {
      throw new \RuntimeException('メッセージ数を取得しようとしたがデータベースの処理に失敗した。');
    }
    $cnt = $stmt->fetchColumn();
    return (int)$cnt;
  }


  public function getBoardData() {
    $sql = $this->makeBoardDataSQL();
    $stmt = $this->pdo->query($sql);
    if (!$stmt) {
      throw new \RuntimeException('掲示板のデータを取得しようとしたがデータベースの処理に失敗した。');
    }
    $data = $stmt->fetch();
    $boardData = new BoardData($data);
    return $boardData;
  }


  public function preesedMessages() {
    if ($this->userId === '-1') {
      return null;
    }
    $likeList = new LikeListFile($this->userId, $this->roomId);
    return $likeList->getPressedMessages();
  }


  public function postMessage($message) {
    if ($this->userId === '-1') {
      throw new myexception\OperationException('ゲストユーザ-ができない操作をしようとしている。');
    }

    if (!$this->isValidInputStr($message)) {
      throw new myexception\InputException('入力された本文が1文字以上の文字列でない。');
    }

    // 投稿
    $postedDate = $this->getNow();
    $sql = $this->makeInsertMessageSQL(false);
    $sql .= $this->makeAddCountSQL();
    $stmt = $this->pdo->prepare($sql);

    $inputParameters = [
      'from_id'     => $this->userId,
      'body'        => $message,
      'posted_date' => $postedDate];
    if (!$stmt->execute($inputParameters)) {
      throw new \RuntimeException('投稿をしようとしたがデータベースの処理に失敗した。');
    }
  }


  // HACK: postMessage($message)と重複している。
  public function replyMessage($message, $replyTo) {
    if ($this->userId === '-1') {
      throw new myexception\OperationException('ゲストユーザ-ができない操作をしようとしている。');
    }

    if (!$this->isValidInputStr($message)) {
      throw new myexception\InputException('入力された本文が1文字以上の文字列でない。');
    }

    // 投稿
    $postedDate = $this->getNow();
    $sql = $this->makeInsertMessageSQL(true);
    $sql .= $this->makeAddCountSQL();
    $sql .= "SELECT @@IDENTITY;";
    $stmt = $this->pdo->prepare($sql);

    $inputParameters = [
      'from_id'     => $this->userId,
      'body'        => $message,
      'posted_date' => $postedDate,
      'to_id'       => $replyTo,
    ];
    if (!$stmt->execute($inputParameters)) {
      throw new \RuntimeException('投稿をしようとしたがデータベースの処理に失敗した。');
    }
    $stmt->nextRowset();
    $stmt->nextRowset();
    $newMessageId = $stmt->fetchColumn();


    // 返信
    $sql = $this->makeUpDateRepliedMessageSQL();
    $inputParameters = [
      'reply_to'   => $replyTo,
      'message_id' => $newMessageId
    ];
    $stmt = $this->pdo->prepare($sql);
    if (!$stmt->execute($inputParameters)) {
      throw new \RuntimeException('返信をしようとしたがデータベースの処理に失敗した。');
    }
  }



  public function pressLike($messageId) {
    // like_list.jsonファイル
    $likeList = new LikeListFile($this->userId, $this->roomId);
    $likeList->pressLike($messageId);
    $likeList->saveLikeListFile();

    // データベース
    $sql = $this->makeAddLikeSQL();
    $inputParameters = [
      'message_id' => $messageId
    ];
    $stmt = $this->pdo->prepare($sql);
    if (!$stmt->execute($inputParameters)) {
      throw new \RuntimeException('いいねしようとしたがデータベースの処理に失敗した。');
    }
  }



  public function getMessages($begine, $range) {
    $sql = $this->makeGetMessageSQL();
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue('begine', (int)$begine, PDO::PARAM_INT);
    $stmt->bindValue('range',  (int)$range,  PDO::PARAM_INT);
    if (!$stmt->execute()) {
      throw new \RuntimeException('データを取得しようとしたがデータベースの処理に失敗した。');
    }

    $messages = $stmt->fetchAll();

    $result = [];
    foreach ($messages as $message) {
      $message_id  = $message["message_id"];
      $from_id     = $message["from_id"];
      $to_id       = $message["to_id"];
      $posted_date = $message["posted_date"];
      $name        = $message["name"];
      $reply       = $message["reply"];
      $body        = $message["body"];
      $like        = $message["like"];
      $temp = new Message($message_id, $from_id, $to_id, $posted_date, $name, $reply, $body, $like);
      if ($temp->isSafe) {
        unset($temp->isSafe);
        $result[] = $temp;
      }
    }
    return $result;
  }

  // 新しいメッセージのレコードの挿入のSQL
  private function makeInsertMessageSQL($isReply) {
    $rows = ['from_id', 'body', 'posted_date'];
    if ($isReply) {
      $rows[] = 'to_id';
    }

    $row   = '(`'.join('`,`', $rows).'`)';
    $value = '(:'.join(', :', $rows).')';
    $sql = "INSERT INTO `".$this->roomId."` ".$row.' VALUES '.$value.';';
    return $sql;
  }

  // 返信先のメッセージのレコードを更新のSQL
  private function makeUpDateRepliedMessageSQL() {
    $sql = "UPDATE `".$this->roomId."` ";
    $sql .= "SET `reply` = CONCAT(IFNULL(`reply`,''), :message_id,',')";
    $sql .= "WHERE `message_id` = :reply_to;";
    return $sql;
  }

  // 掲示板のメッセージ数(cnt)を更新のSQL
  private function makeAddCountSQL() {
    $sql = "UPDATE `discussion_board_master` ";
    $sql .= "SET `last_posted_date` = :posted_date, `cnt` = `cnt` + 1 ";
    $sql .= "WHERE `board_id` = '".$this->roomId."';";
    return $sql;
  }

  // いいねを1追加するのSQL
  private function makeAddLikeSQL() {
    $sql = "UPDATE `".$this->roomId."` ";
    $sql .= "SET `like` = `like` + 1 ";
    $sql .= "WHERE `message_id` = :message_id;";
    return $sql;
  }


  private function makeGetMessageSQL() {
    // 専用テーブルから取得する項目
    $roomRows = ["message_id", "from_id", "to_id", "body", "posted_date", "reply", "like"];

    $sql = "SELECT ";
    foreach ($roomRows as $row) {
      $sql .= "`".$this->roomId."`.`".$row."`, ";
    }
    $sql .= "`user`.`name` ";
    $sql .= "FROM `".$this->roomId."`, `user` ";
    $sql .= "WHERE (CASE WHEN `".$this->roomId."`.`from_id` is NULL THEN '-1' ELSE `".$this->roomId."`.`from_id` END) = `user`.`id` ";
    $sql .= "ORDER BY `".$this->roomId."`.`message_id` DESC ";
    $sql .= "LIMIT :range OFFSET :begine;";
    return $sql;
  }


  private function makeBoardDataSQL() {
    $masterRows = ['board_id', 'board_name', 'created_date', 'last_posted_date', 'master_id', 'cnt', 'description', 'views', 'category'];

    $sql = "SELECT ";
    foreach ($masterRows as $row) {
      $sql .= "`discussion_board_master`.`".$row."`, ";
    }
    $sql .= "`user`.`name` ";
    $sql .= "FROM `discussion_board_master`, `user` ";
    $sql .= "WHERE `discussion_board_master`.`board_id`='".$this->roomId."' ";
    $sql .= "AND `discussion_board_master`.`master_id` = `user`.`id`";
    return $sql;
  }


  private function getNow() {
    return date("Y-m-d H:i:s");
  }


  private function isValidInputStr($input) {
    if (!is_string($input) || preg_match('/^[\s　]*$/', $input)) {
      return false;
    }
    return true;
  }




}
