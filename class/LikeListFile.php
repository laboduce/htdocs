<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Exceptions.php');


class LikeListFile
{
  private $userId;
  private $roomId;
  private $likeList;

  function __construct($userId, $roomId) {
    $this->userId = $userId;
    $this->roomId = $roomId;
    $this->likeList = $this->loadLikeListFile();
  }


  private function loadLikeListFile() {
    $filename = DATA_DIR.'/user/'.$this->userId.'/like_list.json';
    if (!file_exists($filename)) {
      return [];
    }

    $jsonStr = file_get_contents($filename);
    if (!is_string($jsonStr)) {
      return [];
    }

    $likeList = json_decode($jsonStr, true);
    if (!is_array($likeList)) {
      return [];
    }
    return $likeList;
  }


  // すでにいいねしたことがあるか
  private function hasPressedLike($messageId) {
    if (!isset($this->likeList[$this->roomId])) {
      return false;
    }
    if (!in_array($messageId, $this->likeList[$this->roomId], true)) {
      return false;
    }
    return true;
  }


  public function pressLike($messageId) {
    if ($this->hasPressedLike($messageId)) {
      throw new myexception\OperationException('すでにいいねしたことがあるコメントにいいねしようとしている。');
    }

    if (!isset($this->likeList[$this->roomId])) {
      $this->likeList[$this->roomId] = [];
    }
    $this->likeList[$this->roomId][] = $messageId;
  }

  // pressLike()を複数回やる場合のために独立させておいた。
  public function saveLikeListFile() {
    $userDir = DATA_DIR.'/user/'.$this->userId;
    $filename = $userDir.'/like_list.json';
    if (!file_exists($userDir) && !mkdir($userDir)) {
      throw new \RuntimeException('ユーザーディレクトリ('.$userDir.')の作成に失敗。');
    }

    $jsonStr = json_encode($this->likeList);
    if (!is_string($jsonStr)) {
      throw new \RuntimeException('jsonエンコードに失敗。');
    }

    $result = file_put_contents($filename, $jsonStr);
     if ($result === false) {
      throw new \RuntimeException('いいねリスト('.$filename.')の保存に失敗。');
    }
  }

  public function getPressedMessages() {
    if (!isset($this->likeList->{$this->roomId})) {
      return [];
    }
    return $this->likeList->{$this->roomId};
  }



}
