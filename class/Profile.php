<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/MyPDO.php');


class Profile
{
  private $profile;
  private $isMe;

  function __construct($id, $userId=null) {
    $this->isMe = false;

    $this->profile = null;
    if (!is_string($id) || preg_match('/^[\s　]*$/', $id)) {
      return;
    }

    $pdo = new MyPDO();
    $sql = "SELECT `name`, `id`, `comment` FROM `user` WHERE `id` = :id;";
    $stmt = $pdo->prepare($sql);
    if (!$stmt->execute(['id' => $id])) {
      return;
    }
    $profileData = $stmt->fetch();
    if (!is_array($profileData)) {
      return;
    }

    $this->profile['name'] = $profileData['name'];
    $this->profile['id'] = $profileData['id'];
    $this->profile['comment'] = preg_replace('/&#(13|xd);&#(10|xa);/i', "<br>\n", $profileData['comment']);
    $this->profile['iconURL'] = $this->getIconURL();

    if (is_string($userId) && !preg_match('/^[\s　]*$/', $userId)) {
      $this->isMe = $this->profile['id'] === $userId;
    }
  }


  public function getProfile() {
    return $this->profile;
  }

  public function getIsMe() {
    return $this->isMe;
  }

  public function editName($name) {
    if (!$this->isMe) {
      throw new myexception\AuthorityException('自分以外のユーザーのプロフィールを変更しようとしている。');
    }
    if (!is_string($name) || preg_match('/^[\s　]*$/', $name)) {
      throw new myexception\InputException('入力されたユーザー名の値が不適切。($name:'.$name.')');
    }
    if ($name === $this->profile['name']) {
      throw new myexception\OperationException('元のユーザー名と同じユーザー名に変更しようとしている。');
    }

    $pdo = new MyPDO();

    // すでに存在するユーザー名でないことを確認
    $sql = "SELECT EXISTS (SELECT * FROM `user` WHERE `name` = :name);";
    $stmt = $pdo->prepare($sql);
    if (!$stmt->execute(['name' => $name])) {
      throw new \RuntimeException('登録済みのユーザー名か確認しようとしたがデータベースの処理に失敗した。');
    }

    if ($stmt->fetchColumn()) {
      throw new myexception\DBException('登録しようとしたユーザー名がすでに登録されている。', 1001);
    }


    // ユーザー名を変更
    $sql  = "UPDATE `user` SET `name` = :name WHERE `id` = :id;";
    $stmt = $pdo->prepare($sql);
    $parameters = [
      'id'   => $this->profile['id'],
      'name' => $name,
    ];
    if (!$stmt->execute($parameters)) {
      throw new \RuntimeException('ユーザーの変更をしようとしたがデータベースの処理に失敗した。');
    }


    $_SESSION['name'] = $name;
    $this->profile['name'] = $name;
  }

  public function editComment($comment) {
    if (!$this->isMe) {
      throw new myexception\AuthorityException('自分以外のユーザーのプロフィールを変更しようとしている。');
    }
    if (!is_string($comment)) {
      throw new myexception\InputException('入力されたコッメントの値が不適切。($comment:'.$comment.')');
    }

    if (preg_match('/^[\s　]*$/', $comment)) {
      $comment = null;
    }
    if ($comment === $this->profile['comment']) {
      throw new myexception\OperationException('元のコメントと同じコメントに変更しようとしている。');
    }


    $pdo = new MyPDO();
    $sql = "UPDATE `user` SET `comment` = :comment WHERE `id` = :id;";
    $stmt = $pdo->prepare($sql);
    $parameters = [
      'comment' => $comment,
      'id'      => $this->profile['id']
    ];
    if (!$stmt->execute($parameters)) {
      throw new \RuntimeException('コメントの変更をしようとしたがデータベースの処理に失敗した。');
    }


    $this->profile['comment'] = $comment;
  }

  public function editIcon() {
    if (!$this->isMe) {
      throw new myexception\AuthorityException('自分以外のユーザーのプロフィールを変更しようとしている。');
    }

    if (isset($_FILES['image'])) {
      if (!isset($_FILES['image']['error']) || !is_int($_FILES['image']['error'])) {
        throw new myexception\InputException('入力されたファイルが不正。');
      }

      switch ($_FILES['image']['error']) {
        case UPLOAD_ERR_OK:
          if ($_FILES['image']['size'] > 1000000) {
            throw new myexception\InputException('送信されたファイルが大きすぎる。('.$_FILES['image']['size'].')', 1000);
          }
          $this->changeIcon($_FILES['image']['tmp_name']);
          break;

        case UPLOAD_ERR_NO_FILE:
          throw new myexception\InputException('ファイルが送信されなかった。', 1003);
          break;

        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
          throw new myexception\InputException('送信されたファイルが大きすぎる。('.$_FILES['image']['size'].')', 1001);
          break;

        default:
          throw new \RuntimeException('ファイル受信時にエラーが発生した。');
          break;
      }
    } else {
      throw new myexception\InputException('ファイルが送信されなかった。',  1000);
    }
  }



  // TODO: exif情報の変更。位置情報とか。
  private function changeIcon($tempName) {
    $mime = mime_content_type($tempName);
    preg_match('/^image\/(png|jpeg|gif)$/', $mime, $matches);
    if (!is_array($matches) || !isset($matches[1])) {
      throw new myexception\InputException('入力されたファイルがpng・jpeg・gif以外だった。', 1002);
    }

    switch ($matches[1]) {
      case 'png':
        $type ='png';
        break;

      case 'jpeg':
        $type ='jpg';
        break;

      case 'gif':
        $type ='gif';
        break;

      default:
        throw new myexception\InputException('入力されたファイルがpng・jpeg・gif以外だった。', 1002);
    }


    $userDir = DATA_DIR.'/user/'.$this->profile['id'];
    $imageDir = $userDir.'/images';
    $destination = $imageDir.'/icon.'.$type;

    $old = glob($imageDir.'/icon.*');
    if (is_array($old)) {
      foreach ($old as $file) {
        try {
          unlink($file);
        } catch (\Exception $e) {
          continue;
        }
      }
    }

    if (!move_uploaded_file($tempName, $destination)) {
      throw new \RuntimeException('ファイル保存時にエラーが発生した。');
    }
    chmod($destination, 0644);
    $this->profile['iconURL'] = $this->getIconURL();
  }

  private function getIconURL() {
    $icons = glob(DATA_DIR.'/user/'.$this->profile['id'].'/images/icon.{png,jpg}', GLOB_BRACE);
    if (is_array($icons) && count($icons) > 0) {
      $iconUrl = str_replace(DATA_DIR, DATA_URL, $icons[0]);
    } else {
      $iconUrl = DATA_URL.'/user/default.png';
    }
    return $iconUrl;
  }


}
