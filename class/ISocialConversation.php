<?php

interface ISocialConversation
{
  public function setRoomId($roomId);

  public function postMessage($message);

  public function replyMessage($message, $messageId);

  public function pressLike($messageId);

  public function getMessages($begine, $range);

}
