<?php
class Message
{
  public $message_id;
  public $from_id;
  public $to_id;
  public $body;
  public $posted_date;
  public $name;
  public $reply;
  public $isSafe;
  public $like;

  function __construct($message_id, $from_id, $to_id, $posted_date, $name, $reply, $body, $like=null) {
    $this->isSafe = false;
    if (!is_string($message_id) && !is_int($message_id)) {
      return;
    }
    if (!is_string($from_id) && !is_null($from_id) && $from_id !== -1) {
      return;
    }
    if (!is_string($to_id) && !is_null($to_id) && !is_int($message_id)) {
      return;
    }
    if (!is_string($posted_date)) {
      return;
    }
    if (!is_string($name)) {
      return;
    }

    if (is_array($reply)) {
      $replys = $reply;
    } elseif (!is_string($reply) && !is_null($reply)) {
      return;
    } elseif (!is_null($reply)) {
      $replys = explode(',', substr($reply, 0, -1));
    }
    if (!is_string($body)) {
      return;
    }
    $this->isSafe = true;
    $this->message_id = $message_id;
    $this->from_id = $from_id;
    $this->to_id = $to_id;
    $this->body = preg_replace('/&#(13|xd);&#(10|xa);/i', "<br>\n", $body);
    date_default_timezone_set('Asia/Tokyo');
    $this->posted_date = strtotime($posted_date);
    $this->name = $name;
    if (isset($replys)) {
      $this->reply = $replys;
    }
    if (is_int($like) || preg_match('/^\d+$/', $like)) {
      $this->like = (int)$like;
    } else {
      $this->like = 0;
    }
  }
}
