/*
ログイン画面で入力されていない項目を <strong></strong> で強調する。
 */


function setStrong(id) {
  var element = document.getElementById(id);

  if (element.childNodes[0].innerHTML) {
    return;
  }

  var text = element.innerHTML;
  element.removeChild(element.firstChild);
  var addElement = document.createElement("strong");
  addElement.innerHTML = text;
  element.appendChild(addElement);
}


function removeStrong(id) {
  var element = document.getElementById(id);

  if (!element.childNodes[0].innerHTML) {
    return;
  }

  var text = element.childNodes[0].innerHTML
  element.removeChild(element.firstChild);
  element.innerHTML = text;
}


function check() {
  var name = document.forms.login_form.name.value;
  var pw = document.forms.login_form.pw.value;

  if (name === "" || name === null) {
    setStrong("name");
  } else {
    removeStrong("name");
  }

  if (pw === "" || pw === null) {
    setStrong("pw");
  } else {
    removeStrong("pw");
  }

  if (name !== null & pw !== null & name !== "" & pw !== "") {
    var btn = document.forms.login_form.login_button;
    btn.innerHTML = "ログイン<br>お待ちください";
    document.forms.login_form.submit();
  }
}
