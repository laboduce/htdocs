jQuery(function($) {

  var modalwindow = $('#modalwindow');
  var w = $(window).width();
  var h = $(window).height();
  var cw = modalwindow.outerWidth();
  var ch = modalwindow.outerHeight();

  modalwindow.css({
    'left': ((w - cw) / 2) + 'px',
    'top': ((h - ch) / 2) + 'px'
  });



  $('a.profile').on('click', function(e) {
    $('#overlay,#modalwindow').css('display', 'block');

    var idRE = new RegExp('id=([^&]*)');
    var userRE = new RegExp('user=([^&]*)');
    var idArr = idRE.exec($(this).attr('href'));
    var userArr = userRE.exec($(this).attr('href'));
    if (idArr !== null) {
      var inner = '<h1>プロフィール</h1>';
      var data = {id: idArr[1]};
      var relationship = '';
      if (userArr !== null) {
        data.user = userArr[1];
      }
      modalwindow.children('div').empty();
      modalwindow.children('div').append('<h1>通信中...</h1>');

      $.ajax({
        type:'GET',
        url:'/api/profile.php',
        data: data,
        dataType:'json',
      })
      .done((data) => {
        //成功した場合の処理
        if (data['status'] != 200) {
          inner += '<p>エラー</p>'// api の失敗

        } else {
          var result = data['result'];
          if (!result) {
            inner += '<p>ユーザーが見つからなかった。</p>'

          } else {
            console.log(result);
            inner += '<img src="' + result['image'] + '" alt="アイコン">';
            inner += '<div>ユーザー名：' + result['name'] + '</div>';
            inner += '<div>ユーザーID：' + result['id'] + '</div>';
            if (result['comment'] === null) {
              inner += '<div>コメント：<p>コメントがありません。</p></div>';
            } else {
              inner += '<div>コメント：<p>' + result['comment'] + '</p></div>';
            }

            switch (result['relationship']) {
              case 1:
                // 他人
                relationship = '他人';
                break;
              case 2:
                // 自分
                relationship = '自分';
                break;
              case 3:
                // 友達
                relationship = '友達';
                inner += '<div><a href="/user/contents/chat/?t=private&id=' + result['id'] + '">チャット</a></div>';
                break;
              case 4:
                // 申請中
                relationship = '申請中';
                break;
              case 5:
                // 申請されている
                relationship = '申請されている';
                break;
              default:
                // 相手 or 自分がゲスト
                // userId が指定されていない時も
                relationship = 'ゲスト';
            }
            inner += '<div>関係：' + relationship + '</div>';
          }
        }
      })
      .fail((data) => {
        //失敗した場合の処理
        inner += '<p>通信に失敗した。</p>'
      })
      .always((data) => {
        //成功・失敗どちらでも行う処理
        modalwindow.children('div').empty();
        modalwindow.children('div').append(inner);
      });
    }
    e.preventDefault();
  });


  $('.close').on('click', function() {
    $('#overlay,#modalwindow').css('display', 'none');
  });


  modalwindow.on('click', function(e) {
    e.stopPropagation();
  });


});
