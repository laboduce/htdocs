function setStrong(id) {
  var element = document.getElementById(id);

  if (element.childNodes[0].innerHTML) {
    return;
  }

  var text = element.innerHTML;
  element.removeChild(element.firstChild);
  var addElement = document.createElement("strong");
  addElement.innerHTML = text;
  element.appendChild(addElement);
}


function removeStrong(id) {
  var element = document.getElementById(id);

  if (!element.childNodes[0].innerHTML) {
    return;
  }

  var text = element.childNodes[0].innerHTML
  element.removeChild(element.firstChild);
  element.innerHTML = text;
}


function check() {
  var name = document.forms.register_form.name.value;
  var pw = document.forms.register_form.pw.value;
  var email = document.forms.register_form.email.value;

  if (name === "" || name === null) {
    setStrong("name");
  } else {
    removeStrong("name");
  }

  if (pw === "" || pw === null) {
    setStrong("pw");
  } else {
    removeStrong("pw");
  }

  if (email === "" || email === null) {
    setStrong("email");
  } else {
    removeStrong("email");
  }


  if (name !== null & pw !== null & email !== null & name !== "" & pw !== "" & email !== "") {
    document.forms.register_form.submit();
  }
}
