/*
大学・学部・学科の <option> 内容を選択内容に応じて変化させる。
 */


var
univData,
univ = [],
gakubu = {};

var req = new XMLHttpRequest();

function setSelect(idName, elementName, menu){
  var id = document.getElementById(idName);
  while (id.firstChild) {
    id.removeChild(id.firstChild);
  }

  for (var i in menu){
    var element = document.createElement(elementName);
    element.setAttribute('value', i);
    element.innerHTML = menu[i];
    id.appendChild(element);
  }
}

function changeGakubu(){
  var i = document.getElementById('gakubu').selectedIndex;
  setSelect('gakka', 'option', gakubu[Object.keys(gakubu)[i]]);
}

function changeUni(){
  var i = document.getElementById('univ').selectedIndex;
  for (d of univData) {
    if (d["univ"] === univ[i]) {
      if (d["gakubu"]) {
        gakubu = d["gakubu"];
      } else {
        gakubu = {};
      }
    }
  }
  setSelect('gakubu', 'option', Object.keys(gakubu));
  changeGakubu();
}

req.onreadystatechange = function() {
  if(req.readyState == 4 && req.status == 200){
    try {
      univData = JSON.parse(req.responseText);
      for (var d of univData) {
        if (d["univ"]) {
          univ.push(d["univ"]);
          if (univ.length === 1) {
            if (d["gakubu"]) {
              gakubu = d["gakubu"];
            }
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
    setSelect('univ', 'option', univ);
    setSelect('gakubu', 'option', Object.keys(gakubu));
    setSelect('gakka', 'option', gakubu[Object.keys(gakubu)[0]])
  }
};
req.open("get", "/data/univ.json", true);
req.send(null);
