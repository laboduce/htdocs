jQuery(function($){
  $('div.reply').on('click', function(){
    var messageId = $(this).parent().attr('id');
    var re = new RegExp('id=([^&]*)');
    var arr = re.exec(location.search);
    if (arr !== null) {
      var data = {
        id: arr[1],
        message: $(this).parent().attr('id'),
        // r: 2,
      };
      $.ajax({
        type: 'GET',
        url: '/api/replies.php',
        data: data,
        dataType: 'json',
      })
      .done((data) => {
        if (data.status !== 200) {
          // TODO: エラー
        } else {
          var text = 'No.' + messageId + 'に対する返信(再帰)';
          for (key in data.result) {
            text += '\nNo.' + key;
          }
          alert(text);

        }
      })
      .fail((data) => {

      })
      .always((data) => {

      });
    }
  });
})
