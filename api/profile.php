<?php
define('GUEST', 0);
define('STRANGER', 1);
define('MYSELF', 2);
define('FRIEND', 3);
define('REQUESTING', 4);
define('REQUESTED', 5);

require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/Profile.php');
require_once(CLASS_DIR.'/Friends.php');

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$userId = filter_input(INPUT_GET, 'user', FILTER_SANITIZE_SPECIAL_CHARS);
$data['message'] = null;
$data['result'] = null;
$data['status'] = 500;

// 200: 正常
// 400: リクエストエラー
// 404: 存在しない
// 500: API内部で発生したエラー

if (!is_string($id) || $id==='') {
  $data['message'] = 'idが指定されていません。';
  $data['status'] = 400;

} else {
  $data['status'] = 200;

  $p = new Profile($id);
  $profile = $p->getProfile();
  if (is_null($profile)) {
    $data['message'] = 'idに対応するデータが見つかりませんでした。';
    $data['status'] = 404;
  } else {
    if (isset($profile['id']) && is_string($profile['id'])) {
      $data['result']['id'] = $profile['id'];
    } else {
      $data['result']['id'] = null;
    }

    if (isset($profile['name']) && is_string($profile['name'])) {
      $data['result']['name'] = $profile['name'];
    } else {
      $data['result']['name'] = null;
    }

    if (isset($profile['comment']) && is_string($profile['comment'])) {
      $data['result']['comment'] = $profile['comment'];
    } else {
      $data['result']['comment'] = null;
    }

    $data['result']['image'] = $profile['iconURL'];

    if (is_string($userId) && !preg_match('/^[\s　]*$/', $userId)) {
      if ($id ==='-1' || $userId === '-1') {
        $data['result']['relationship'] = GUEST;

      } elseif ($id === $userId) {
        $data['result']['relationship'] = MYSELF;

      } else {
        $friend = new Friends($userId);
        if ($friend->getFriendsData() !== false) {
          if (getRelationship($friend->getFriends(), $id)) {
            $data['result']['relationship'] = FRIEND;
          } elseif (getRelationship($friend->getRequestingFriend(), $id)) {
            $data['result']['relationship'] = REQUESTING;
          } elseif (getRelationship($friend->getRequestedFriend(), $id)) {
            $data['result']['relationship'] = REQUESTED;
          } else {
            $data['result']['relationship'] = STRANGER;
          }
        } else {
          $data['message'] = 'userに対応するデータが見つかりませんでした。';
          $data['result'] = null;
          $data['status'] = 404;
        }
      }
    }

  }
}


function getRelationship($array, $id) {
  if (!is_array($array)) {
    return false;
  }
  foreach ($array as $user) {
    if ($user->id === $id) {
      return true;
    }
  }
  return false;
}



$json = json_encode($data, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
print($json);
