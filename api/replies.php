<?php
define('DEFAULT_DEPTH', 100);
require_once($_SERVER['DOCUMENT_ROOT'].'/data/config.php');
require_once(CLASS_DIR.'/MyPDO.php');
require_once(CLASS_DIR.'/Exceptions.php');
$boardId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$messageId = filter_input(INPUT_GET, 'message', FILTER_SANITIZE_SPECIAL_CHARS);
$depth = filter_input(INPUT_GET, 'r', FILTER_SANITIZE_SPECIAL_CHARS);

$data['message'] = null;
$data['result'] = null;
$data['status'] = 500;

if (!is_string($boardId) || preg_match('/^[\s　]*$/', $boardId)) {
  $data['message'] = 'idが指定されていません。';
  $data['status'] = 400;

} elseif (!is_string($messageId) || preg_match('/^[\s　]*$/', $messageId) || !preg_match('/^[1-9]\d*$/', $messageId)) {
  $data['message'] = 'messageを整数で指定してください。';
  $data['status'] = 400;

} elseif (is_string($depth) && !preg_match('/^[\s　]*$/', $depth) && !preg_match('/^[1-9]\d*$/', $depth)) {
  $data['message'] = '深さrは整数値です。';
  $data['status'] = 400;

} else {
  $data['status'] = 200;
  if (!is_string($depth) || preg_match('/^[\s　]*$/', $depth)) {
    $depth = DEFAULT_DEPTH;
  }

  $pdo = new MyPDO();
  $message = false;
  try {
    $message = getMessage($pdo, $boardId, $messageId);
  } catch (myexception\DBException $e) {
    $data['message'] = '掲示板が見つかりませんでした。';
    $data['status'] = 404;
  } catch (myexception\OperationException $e) {
    $data['message'] = 'メッセージが見つかりませんでした。';
    $data['status'] = 404;
  } catch (\Exception $e) {
    $data['status'] = 500;
  }

  if ($message) {
    // $data['result'][$message['message_id']] = $message; // TODO: 自分自身は？？
    $replies = $message['reply'];
    $toIds = array_pad([], count($replies), $messageId);
    $replyId = array_pop($replies);
    $toId = array_pop($toIds);

    for ($i=0; $i < $depth; $i++) {
      $nextReplies = [];
      $nextToId = [];
      while ($replyId) {
        if (!isset($data['result'][$replyId])) {
          try {
            $message = getMessage($pdo, $boardId, $replyId);
            $data['result'][$replyId] = $message;
            $nextReplies  = array_merge($nextReplies, $message['reply']);
            $nextToId = array_pad($nextToId, count($nextReplies), $message['message_id']);
          } catch (\Exception $e) {
            $message = [];
            $message['message_id'] = $replyId;
            $message['to_id'] = [$toId];
            $message['status'] = 0;

            $data['result'][$replyId] = $message;
          }
        }
        $replyId = array_pop($replies);
        $toId = array_pop($toIds);
      }
      $replies = $nextReplies;
      $toIds = $nextToId;
      $replyId = array_pop($replies);
      $toId = array_pop($toIds);
    }
  }
}





function getMessage($pdo, $roomId, $messageId) {
  $sql = "SELECT * FROM `".$roomId."` WHERE `message_id` = :message_id;";
  $stmt = $pdo->prepare($sql);
  $inputParameters = [
    'message_id' => $messageId,
  ];
  if (!$stmt->execute($inputParameters)) {
    throw new myexception\DBException('掲示板が見つからなかった。');
  }
  $messageData = $stmt->fetch();

  $message = [];
  $message['message_id'] = $messageData['message_id'];
  $message['from_id'] = $messageData['from_id'];

  if (is_string($messageData['to_id'])) {
    $toId = $messageData['to_id'];
    $toId = $toId[-1]===',' ? substr($toId, 0, -1) : $toId;
    $message['to_id'] = explode(',', $toId);
  } else {
    $message['to_id'] = [];
  }
  $message['body'] = $messageData['body'];
  $message['posted_date'] = $messageData['posted_date'];
  if (is_string($messageData['reply'])) {
    $reply = $messageData['reply'];
    $reply = $reply[-1]===',' ? substr($reply, 0, -1) : $reply;
    $message['reply'] = explode(',', $reply);
  } else {
    $message['reply'] = [];
  }
  $message['like'] = $messageData['like'];
  $message['status'] = 1;

  return $message;
}

// messageごと
// 1: 正常
// 0: 見つからなかった

// 全体
// 200: 正常
// 400: リクエストエラー
// 404: 存在しない
// 500: API内部で発生したエラー


$json = json_encode($data, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
print($json);
